package Chapter1.AccessModifiers.Animal.DomesticAnimals;

import Chapter1.AccessModifiers.Animal.Animal;

public class Dog extends Animal {

    public void dogBark() {

        Animal a = new Animal();
//        a.makeNoise();
//        a.numberOfLegs();
        a.eat();
        //
        //
        System.out.println("Dog Bark");
    }


    @Override
    public void eat() {
        super.eat();
    }

    @Override
    protected int numberOfLegs() {
        return super.numberOfLegs();
    }
}
