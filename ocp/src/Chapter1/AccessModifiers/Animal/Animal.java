package Chapter1.AccessModifiers.Animal;


public class Animal {

    public void eat() {

        System.out.println("Animal Eating");

    }

    void makeNoise() {
        System.out.println("Animal Making Noise");
    }

    protected int numberOfLegs() {
        System.out.println("number of legs");
        return 0;
    }
}
