package Chapter1.AccessModifiers.Animal.WildAnimals;

import Chapter1.AccessModifiers.Animal.Animal;

class Wolf extends Animal {

    @Override
    public void eat() {
        super.eat();
    }

    @Override
    protected int numberOfLegs() {
        return super.numberOfLegs();
    }


}
