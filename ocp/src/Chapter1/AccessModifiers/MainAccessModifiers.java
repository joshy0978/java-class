package Chapter1.AccessModifiers;

import Chapter1.AccessModifiers.Animal.Animal;
import Chapter1.AccessModifiers.Animal.DomesticAnimals.Dog;

public class MainAccessModifiers {


    public static void main(String[] args) {


        Animal a = new Animal();

        a.eat();

        //Wolf w = new Wolf();

        Dog d = new Dog();

        //d.dogBark();

    }
}
