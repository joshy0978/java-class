package Chapter1;

/********************
 *
 *
 * default toString in Object returns something like: com.freniche.ocp8.Person@511d50c0
 * com.freniche.ocp8.Person Full Qualified Class Name
 * 511d50c0: hashcode
 */

class Student  {
    String name;
    String address;
    int age;

    @Override
    public String toString() {
        return super.toString() + " " + super.hashCode() + " " + name + " " + address + " " + age;
    }

//    correct override
//    receives an Object (can't change argument types on overridden methods)
//    @Override
//    public boolean equals(Student obj) { // nope
//        return super.equals(obj);
//    }

    @Override
    public boolean equals(Object obj) {

        if (obj == this) {
            return true;
        }

        if (obj instanceof Student) {
            Student p = (Student)obj;

            return (p.name.equals(this.name) && address.equals(p.address) && age == p.age);
        }

        return false;
    }

//  hashcode
    //used for maps like HashMap, HashTable, HashSet
    @Override
    public int hashCode() {
        int salt = 37;
        return 31 * (salt + name.hashCode()) * (salt + address.hashCode()) + age;
    }

}
