package Chapter1;

/*****************
 *
 * Using instanceof:
 * reference instanceof Type
 *
 * is this reference pointing to an object that in the heap passes the IS-A test?
 * Type can be: Class, Interface, Enum
 *
 *
 */

interface Compress { void compress(); }

interface Decompress { public abstract void decompress(); }

abstract class Compressor implements Compress, Decompress {}


class ZipCompressor extends Compressor {

    @Override
    public void compress() {

    }

    @Override
    public void decompress() {

    }
}


class Animal { }

class Dog3 extends Animal {

}


public class InstanceOf {

public static void main(String[] args) {

//        instanceof Object is always true
        if (new Object() instanceof Object) {
            System.out.println("Instance of Object!");
        }

//        instanceof compares with a type
//        if (new Object() instanceof null) {         // Error: null is not a type
//            System.out.println("Instance of Object!");
//        }

        if (new Object() instanceof String) {       // String is a type
            System.out.println("Instance of String!");
        }

        if (new Integer[0] instanceof Integer[]) {  // Integer[] is a type
            System.out.println("Instance of array!");
        }

//      what does this print?
        String s = null;

        if (s instanceof String) {
            System.out.println("It's a string");
        } else {
            System.out.println("It's NOT a string");
        }

//        ^ null is not a String instance


//        Correct way of use instanceof
        s = "";

        if (s != null && s instanceof String) {
            System.out.println("It's a string");
        } else {
            System.out.println("It's NOT a string");
        }

//        Null is not an instance of anything
        if (null instanceof Void) {
            System.out.println("null is Void");
        }


        //Down Casting
        Animal a=new Dog3();
        if (a instanceof Dog3) {
            Dog3 d = (Dog3) a;//downcasting
            System.out.println("ok downcasting performed");
        }


        Compressor cc = new ZipCompressor();
        if (cc instanceof ZipCompressor) {
            System.out.println("zc");          // true
        }

        if (cc instanceof Compressor) {
            System.out.println("Compressor");  // true
        }

        if (cc instanceof Compress) {
            System.out.println("Compress");   // true
        }

        if (cc instanceof Decompress) {
            System.out.println("Decompress"); // true
        }

        //Since this can never be true
        //the compiler throws a error
        //String is Final meaning it cannot be subclassed
//        if (cc instanceof String) {         // Error: does not compile
//            System.out.println("Decompress");
//        }


    }

}
