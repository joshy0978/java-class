
package Chapter1;


@FunctionalInterface
interface Printable {
    void print();
}


/**************
 *
 * Can be Empty
 *
 */


enum Day {
}


//Enums can have a list of "values"
//a class that contains a certain number of objects
//You cannot extend Enums but can implement interfaces

//The Enums are required to have exclusively private constructors,
//this is because the Enum should be the only one responsible for
// returning the predefined instances.


enum Months implements Printable{
    JAN(31) {
        @Override
        public void print() {
            System.out.println("January 31st");
        }
    }, FEB, MAR(31), APR;  // semicolon important here!

    int numDays;

    private Months(int numDays) {
        this.numDays = numDays;
    }

    private Months(){
        this.numDays = 0;
    }

    @Override
    public String toString() {
        return super.toString().toLowerCase();
    }


    @Override
    public void print() { //default implementation
        System.out.println("Do nothing");
    }
}




 class Main {


    public static void main(String[] args) {

        Months apr = Months.APR;
        Months m = Months.valueOf("JAN"); // OK

//        m = Months.valueOf("JAn"); // java.lang.IllegalArgumentException: No enum constant

        for (Months month: Months.values()) {
            System.out.println(month.name() + " " + month.ordinal());
        }


        System.out.println(Months.JAN instanceof Months);

        Months.JAN.print();

        Months.FEB.print();

        System.out.println(Months.JAN.numDays);
        System.out.println(Months.FEB.numDays);



    }

}
