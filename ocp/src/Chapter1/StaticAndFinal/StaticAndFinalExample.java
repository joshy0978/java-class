package Chapter1.StaticAndFinal;

/****************
 *
 * Defining a static variable
 * static data_type variable_name;
 *
 */

class Test {

    // Since i is static final,
    // it must be assigned value here
    // or inside static block .
    static final int i;

    static
    {
        i = 10;
    }

}

class VariableDemo
{
    static int count=0;

    //What happens when you DO NOT  initialize  MY_VAR?
    public static final int MY_VAR=27;

    public void increment()
    {
        count++;
    }
    public static void main(String args[])
    {
        VariableDemo obj1=new VariableDemo();
        VariableDemo obj2=new VariableDemo();
        obj1.increment();
        obj2.increment();
        System.out.println("VariableDemo count: is="+VariableDemo.count);

        System.out.println("MY_VAR: " + VariableDemo.MY_VAR);
    }
}

class JavaExample{
    static int age;
    static String name;
    //This is a Static Method
    static void disp(){
        System.out.println("Age is: "+age);
        System.out.println("Name is: "+name);
    }
    // This is also a static method
    public static void main(String args[])
    {
        age = 30;
        name = "Steve";
        disp();
    }
}
