package Chapter1;

interface X{
    void a();
    void b();
    void c();
    void d();
}

abstract class C implements X{
    public void c(){System.out.println("I am c");}
    public abstract void y();
}

class M extends C{
    public void a(){System.out.println("I am a");}
    public void b(){System.out.println("I am b");}
    public void d(){System.out.println("I am d");}
    public void y(){ System.out.println("I am y");}
}

class Test{
    public static void main(String args[]){
        X x=new M();
        x.a();
        x.b();
        x.c();
        x.d();
    }}