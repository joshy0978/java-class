package Chapter1;

/************************************
 *
 * Notes:
 *
 *  we can't override fields, so no "Field virtual whatever" here
 */


class Person_ {
    public String name = "Chapter1.Person name";

    void printName() {
        System.out.println(name);
    }
}

class Worker_ extends Person_ {
    public StringBuffer name = new StringBuffer("Chapter1.Worker_ name");

    void printName() {
        System.out.println(name);
    }

    void methodA(){}
}

class Main_Overloading {

    public static void main(String[] args) {

        // What does this print?

        Person_ p = new Person_();
        p.printName();      // Chapter1.Person name: the object in memory IS-Chapter1.A Chapter1.Person

        Worker_ w = new Worker_();
        w.printName();      // Chapter1.Worker_ name: the object in memory IS-Chapter1.A Chapter1.Worker_

        Person_ p2 = new Worker_();
        p2.printName();     // Chapter1.Worker_ name: the object in memory IS-Chapter1.A Chapter1.Worker_
        ((Worker_) p2).methodA();
//        Chapter1.Worker_ w2 = new Chapter1.Person_();
//        w2.printName();     // Compiler error!


        Person_ p1 = new Person_();
        System.out.println(p.name);     // Chapter1.Person name

        Worker_ w1 = new Worker_();
        System.out.println(w.name);     // Chapter1.Worker name

        Person_ p3 = new Worker_();
        System.out.println(p2.name);   // Chapter1.Person name

    }

}

/**********************************
 *
 * Overridding: Covariant Returns
 *
 *  Java program to demonstrate that we can have
 *  different return types if return type in
 *  overridden method is sub-type
 *
 */



// Two classes used for return types.
class A {}
class B extends A {}
class D  extends B{}

class Base
{
    A fun()
    {
        System.out.println("Chapter1.Base fun()");
        return new A();
    }
}

class Derived extends Base
{
    //B IS-A A
    B fun()
    {
        System.out.println("Chapter1.Derived fun()");
        return new B();
    }
}

class DerivedFromBase extends Base {

    //D IS-A A
    D fun()
    {
     return new D();
    }
}

class Main_
{
    public static void main(String args[])
    {
        Base base = new Base();
        base.fun();

        Derived derived = new Derived();
        derived.fun();
    }
}


/****************************
 *
 * Overload Rules Example
 *
 */

class Logger_ {

    static void printValue(int i) {
        System.out.println(i + " is passed in an int");
    }

    static void printValue(long i) {
        System.out.println(i + " is passed in an long");
    }

    static void printValue(float i) {
        System.out.println(i + " is passed in an float");
    }

    static void printValue(Integer i) {
        System.out.println(i + " is passed in an Integer");
    }

    static void printValue(int ... i) {
        System.out.println(i + " is passed in as an int array by vargs");
    }
    static void printValue(Object i) {
        System.out.println(i + " is passed in an Object");
    }




}

class main_Logger_ {

    public static void main(String[] args) {

        int i = 22;

        float f = 22.3f;

        double d = 22.3;

        //Logger_.printValue(i);

        Logger_.printValue(f);


        //Why is d passed into printValue as an Object?
        //Logger_.printValue(d);
        

    }


}
