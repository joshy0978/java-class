package Chapter1.NestedClasses.MemberClass;

/************************
 *
 * Member Inner Classes
 *  can be declared public, private, or protected or use default access
 *  Can extend any class and implent interfaces
 *  can be abstract or final
 *  cannont declare static fields or methods
 *  can access memebers of the outer class including private members
 *
 *
 */

class Car {
    String carName;
    String carType;
    public Car(String name, String type) {
        this.carName = name;
        this.carType = type;
    }
    public String getCarName() {
        return this.carName;
    }
    class Engine {
        String engineType;
        String getEngineType(){
            return this.engineType;
        }
        void setEngine() {
            // Accessing carType property of Car
            if(Car.this.carType.equals("4WD")){
                // Invoking method getCarName() of Car
                if(Car.this.getCarName().equals("Crysler")) {
                    this.engineType = "Bigger";
                } else {
                    this.engineType = "Smaller";
                }
            }else{
                this.engineType = "Bigger";
            }
        }
    }
}


class CarMain {
    public static void main(String[] args) {
        Car car1 = new Car("Mazda", "8WD");
        Car.Engine engine = car1.new Engine();
        engine.setEngine();
        System.out.println("Engine Type for 8WD = " + engine.getEngineType());

        Car car2 = new Car("Crysler", "4WD");
        Car.Engine c2engine = car2.new Engine();
        c2engine.setEngine();
        System.out.println("Engine Type for 4WD = " + c2engine.getEngineType());
    }
}
