package Chapter1.NestedClasses.MemberClass;

/************************
 *
 * Member Inner Classes
 *  can be declared public, private, or protected or use default access
 *  Can extend any class and implent interfaces
 *  can be abstract or final
 *  cannont declare static fields or methods
 *  can access memebers of the outer class including private members
 *
 *
 */

class CPU {

    private double price;

    class Processor{
        double cores;
        String manufacturer;
        double getCache(){
            return 4.3;
        }
    }

    protected class Ram{
        double memory;
        String manufacturer;
        double getClockSpeed(){
            return 5.5;
        }
    }

    public void callProcessor(){
        Processor processor = new Processor();
        System.out.println(processor.getCache());
    }

    public void callRam(){

        Ram ram = new Ram();
        System.out.println(ram.getClockSpeed());

    }

}
 class Main_MemberClass_1 {
    public static void main(String[] args) {
        CPU cpu = new CPU();
        CPU.Processor processor = cpu.new Processor();
        CPU.Ram ram = cpu.new Ram();
        System.out.println("Processor Cache = " + processor.getCache());
        System.out.println("Ram Clock speed = " + ram.getClockSpeed());
    }
}
