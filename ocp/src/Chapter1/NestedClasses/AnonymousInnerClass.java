package Chapter1.NestedClasses;

/***********************
 *
 * Can be used as implementer of the specifiec interface
 *
 *
 */

class Flavor2Demo {

    // An anonymous class that implements Hello interface
    static Hello h = new Hello() {
        public void show() {
            System.out.println("i am in anonymous class");
        }
    };

    static Hello a = new Hello() {
        @Override
        public void show() {

        }
    };

    static Hello b = () -> {
        System.out.println("");
    };

    public static void main(String[] args) {
        Flavor2Demo.h.show();
    }
}

interface Hello {
    void show();
}