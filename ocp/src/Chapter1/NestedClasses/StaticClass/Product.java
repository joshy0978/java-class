public class Product {
	private String name;
	private Double retailPrice;
	private Double buyPrice;
	private Double wholesalePrice;
	
	public static class Builder{
		private String name;
		private Double retailPrice;
		private Double buyPrice;
		private Double wholesalePrice;
		
		public Builder setName(String name) {
			this.name = name;
			return this;
		}
		
		public Builder setRetailPrice(Double retailPrice) {
			this.retailPrice = retailPrice;
			return this;
		}
		
		public Builder setBuyPrice(Double buyPrice) {
			this.buyPrice = buyPrice;
			return this;
		}
		
		public Builder setWholesalePrice(Double wholesalePrice) {
			this.wholesalePrice = wholesalePrice;
			return this;
		}
		
		public Product build(){
			return new Product(this);
		}
	}
	
	private Product(Builder builder){
		this.name = builder.name;
		this.retailPrice = builder.retailPrice;
		this.buyPrice = builder.buyPrice;
		this.wholesalePrice = builder.wholesalePrice;
	}
 
	public Double getRetailPrice() {
		return retailPrice;
	}
	// getters...
}

class Main {
	public static void main(String[] args) {
		Product product = new Product.Builder()
			.setName("MotoG")
			.setRetailPrice(100.12)
			.build();
	}
}