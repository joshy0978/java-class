package Chapter1.NestedClasses.StaticClass;

class MotherBoardStatic {
    String model;
    public MotherBoardStatic(String model) {
        this.model = model;
    }
    static class USB{
        int usb2 = 2;
        int usb3 = 1;
//        int getTotalPorts(){
//            if(MotherBoardStatic.this.model.equals("MSI")) {
//                return 4;
//            }
//            else {
//                return usb2 + usb3;
//            }
//        }
    }
}
class Main_Static1 {
    public static void main(String[] args) {
        MotherBoardStatic.USB usb = new MotherBoardStatic.USB();
        //System.out.println("Total Ports = " + usb.getTotalPorts());
    }
}