package Chapter1.NestedClasses.StaticClass;


/****************
 *
 *static nested class cannot access the member variables of the
 * outer class because static nested class doesn't require you
 * to create an instance of outer class.
 *
 * Hence, no reference of the outer class exists with OuterClass.this
 *
 * OuterClass.InnerClass obj = new OuterClass.InnerClass();
 *
 */

class MotherBoard {
    String model;
    public MotherBoard(String model) {
        this.model = model;
    }
    static class USB{
        int usb2 = 2;
        int usb3 = 1;
        int getTotalPorts(){
            return usb2 + usb3;
        }
    }
}
class Main_static {
    public static void main(String[] args) {
        MotherBoard.USB usb = new MotherBoard.USB();
        System.out.println("Total Ports = " + usb.getTotalPorts());
    }
}
