package Chapter1.NestedClasses.LocalInnerClass;


/*************************
 *
 * Method local inner class can’t be marked as private, protected, static
 * and transient but can be marked as abstract and final, but not both at the same time.
 */

class Outer {
    void outerMethod() {
        System.out.println("inside outerMethod");
        // Inner class is local to outerMethod()
        class Inner {
            void innerMethod() {
                System.out.println("inside innerMethod");
            }
        }
        Inner y = new Inner();
        y.innerMethod();
    }
}
class MethodDemo {
    public static void main(String[] args) {
        Outer x = new Outer();
        x.outerMethod();
    }
}
