package Chapter1;

/*******************************
 *
 * Notes
 *
 * you can't override a static method: you can hide it
 * you can't override a field: you can redeclare it
 * you can't override a constructor: you don't even inherit constructors
 *
 */

class Person {

    public String name;

    public static Person createPerson(String name) {
        Person p = new Person(name);
        p.name = name;
        return p;
    }

    public Person(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String prettyPrint(String someString) {
        return someString.concat(name);
    }

}

class Worker extends Person {

    public StringBuffer name;


    public Worker(String name) {
        super(name);
    }

    // if we remove this constructor we get error: "There's no default constructor for class Chapter1.Worker_"
    // we don't even "inherit" the default constructor
    // it's injected by the compiler
    //
    // public Chapter1.Worker_(String name) {
    //     super(name);
    // }

    public static Person createPerson(String name) {
        Person p = new Person(name);
        p.name = "Chapter1.Worker_ " + name;
        return p;
    }

    @Override
    public String getName(){
        return name.toString();
    }
}


class Main_Overriding {

    public static void main(String[] args) {


        Person p = new Person("Chapter1.Person Chapter1.A");
        p.name = "Diego";   // String

//        Chapter1.Worker w = new Chapter1.Worker("Chapter1.Worker");
//        w.name = "Chapter1.Worker_";  // StringBuffer: error

        Person p2 = new Worker("Chapter1.Person C");
        p2.name = "P2"; // String

    }
}