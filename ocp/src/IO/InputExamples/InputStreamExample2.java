package IO.InputExamples;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public class InputStreamExample2 {

    public static void main(String[] args) throws IOException {

        InputStream inputstream = new FileInputStream("/Users/joshuataylor/git/java-class/ocp/src/IO/file.txt");

        int data = inputstream.read();
        while(data != -1) {
            //do something with data...

            data = inputstream.read();
            char aChar = (char) data;
            System.out.print(aChar);
        }
        inputstream.close();

    }
}

class InputStreamExample3 {

    public static void main(String[] args) throws IOException {

        InputStream inputstream = new FileInputStream("/Users/joshuataylor/git/java-class/ocp/src/IO/file.txt");

        byte[] data      = new byte[1024];
        int    bytesRead = inputstream.read(data);

        while(bytesRead != -1) {

            if (bytesRead >= 1024) {
                for (int i = 0; i < data.length; i++) {
                    System.out.print((char) data[i]);
                }
            } else {
                for (int i = 0; i < bytesRead; i++) {
                    System.out.print((char) data[i]);
                }
            }

            bytesRead = inputstream.read(data);
        }
        inputstream.close();

    }
}
