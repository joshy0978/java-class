package IO.InputExamples;

import java.io.*;

public class InputStreamExample {

    public static void main(String[] args) {

        InputStream input;
        String line = null;
        try {
            input = new FileInputStream("/Users/joshuataylor/git/java-class/ocp/src/IO/file.txt");
            // wrap InputStreamReader in BufferedReader
            BufferedReader buff = new BufferedReader(new InputStreamReader(input));
            while (buff.ready()) {
                // reads each line of file
                line = buff.readLine();
                System.out.println(line);
            }
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
