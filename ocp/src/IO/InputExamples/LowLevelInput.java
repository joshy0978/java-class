package IO.InputExamples;

import java.io.ByteArrayInputStream;
import java.io.FileOutputStream;

public class LowLevelInput {

    public static void main(String[] args) {

        /************
         *
         * FileOutputStream
         * public class FileOutputStream extends OutputStream
         *
         */


        try{
            FileOutputStream fout=new FileOutputStream("D:\\testout.txt");
            String s="Welcome to javaTpoint.";
            byte b[]=s.getBytes();//converting string into byte array
            fout.write(b);
            fout.close();
            System.out.println("success...");
        }catch(Exception e){System.out.println(e);}


        /****************
             *
             *public class ByteArrayInputStream extends InputStream
             *
             */

        byte[] buf = {35, 36, 37, 38};
        // Create the new byte array input stream
        ByteArrayInputStream byt = new ByteArrayInputStream(buf);
        int k = 0;
        while ((k = byt.read()) != -1) {
            //Conversion of a byte into character
            char ch = (char) k;
            System.out.println("ASCII value of Character is:" + k + "; Special character is: " + ch);


        }

    }

}
