package IO.UserInput;

import java.io.BufferedReader;
import java.io.Console;
import java.io.IOException;
import java.io.InputStreamReader;

public class Input {

    public static void main(String[] args) throws IOException {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Please enter value:");
        String userInput = reader.readLine();
        System.out.println("You entered the following: " + userInput);

        System.out.println("Please enter value:");
        userInput = reader.readLine();
        System.out.println("You entered the following: " + userInput);

    }
}
