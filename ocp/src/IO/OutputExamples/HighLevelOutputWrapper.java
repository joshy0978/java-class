package IO.OutputExamples;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class HighLevelOutputWrapper {

    FileOutputStream fileOutputStream;


    HighLevelOutputWrapper(String path) throws FileNotFoundException{
        this.fileOutputStream = new FileOutputStream("/Users/joshuataylor/git/java-class/ocp/src/IO/OutputExamples/testout.txt");

    }

    void writeString(String output){
        byte b[]=output.getBytes();//converting string into byte array
        try{
            fileOutputStream.write(b);
        } catch(IOException e) {
            System.out.println("Writing to file failed");
        }
    }

    void writeStringOnThread(String output){

            Runnable task = () -> this.writeString(output);
            (new Thread(task)).start();

    }

    void closeStream() {
        try{
            fileOutputStream.close();
        } catch (IOException e){
            System.out.println("Problem closing stream");
        }
    }

    public static void main(String[] args) throws FileNotFoundException {

        String filePath = "/Users/joshuataylor/git/java-class/ocp/src/IO/OutputExamples/testout.txt";


        HighLevelOutputWrapper highLevelOutputWrapper = new HighLevelOutputWrapper(filePath);

        highLevelOutputWrapper.writeString("Hello My name is Josh");

        highLevelOutputWrapper.writeString("This is a test");


        highLevelOutputWrapper.closeStream();

    }


}
