package IO.OutputExamples;

import java.io.*;

class LowLevelOutput {
    public static void main(String args[]) throws FileNotFoundException, IOException {

        /******
         *
         * FileOutputStream
         *
         */
        FileOutputStream fout = new FileOutputStream("/Users/joshuataylor/git/java-class/ocp/src/IO/OutputExamples/testout.txt");

        fout = new FileOutputStream("/Users/joshuataylor/git/java-class/ocp/src/IO/OutputExamples/testout.txt");
        String s = "Welcome to Harper.";
        byte b[] = s.getBytes();//converting string into byte array
        fout.write(b);
        System.out.println("success...");

        fout.close();

        /***********
         *
         * ByteArrayOutputStream
         *
         */

        ByteArrayOutputStream bOutput = new ByteArrayOutputStream(20);

        while (bOutput.size() != 15) {
            // Gets the inputs from the user
            bOutput.write("hello".getBytes());
        }
        byte bytes[] = bOutput.toByteArray();
        System.out.println("Print the content");

        for (int x = 0; x < bytes.length; x++) {
            // printing the characters
            System.out.print((char) bytes[x] + " ");
        }
        System.out.println("   ");


    }
}
