package IO.OutputExamples;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

public class QueueWriter {

    Writer fileWriter;
    List<String> messageList;

    QueueWriter(Writer fileWriter) {
        this.fileWriter = fileWriter;
        messageList = new ArrayList<>();
    }

    void writeToFile(String message) throws IOException {

        fileWriter.write(message);
        fileWriter.flush();
    }

    void queueMessageToWrite(String message) {
        messageList.add(message);
    }

    void writeMessages() {

        Thread thread = new Thread(() -> {

            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            messageList.stream()
                    .forEach((s -> {
                        try {

                            fileWriter.write(s);
                            fileWriter.write("\n");
                            fileWriter.flush();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }));

            System.out.println(Thread.currentThread().getName());

        });


        thread.setName("Writing Thread");
        thread.start();


    }

    void writeMessagesWithoutThread() throws InterruptedException {

        Thread.sleep(5000);

        messageList.stream()
                .forEach((s -> {
                    try {
                        fileWriter.write(s);
                        fileWriter.write("\n");
                        fileWriter.flush();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }));

        System.out.println(Thread.currentThread().getName());


    }
}
