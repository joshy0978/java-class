package IO;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;

public class FileBasic {

    public static void main(String[] args) {
        //First
        File file = new File("/Users/joshuataylor/git/java-class/ocp/src/IO/file.txt");

        System.out.println("Does file one exist: " + file.exists());

        System.out.println("File name: " + file.getName());

        System.out.println("Is File object a directory: " + file.isDirectory());

        System.out.println("Is File object a file: " + file.isFile());

        System.out.println("Size of first file object: " + file.length() + " bytes");

        System.out.println("Fist file was last modified: " + LocalDateTime.ofInstant(Instant.ofEpochMilli(file.lastModified()), ZoneId.systemDefault()));

        //Second
        File parent = new File("/Users/joshuataylor/git/java-class/ocp/src/");
        File file2 = new File(parent, "IO/file.txt");

        System.out.println("Does file parent exist: " + parent.exists());
        System.out.println("Does file file2 exist: " + file2.exists());

        //Third
        File file3 = new File("/Users/joshuataylor/git/java-class/ocp/src/", "IO/file.txt");

        System.out.println("First : "+file.getAbsolutePath());
        System.out.println("Second : "+file2.getAbsolutePath());
        System.out.println("Third : "+file3.getAbsolutePath());

        //Rename file
        File newName = new File("/Users/joshuataylor/git/java-class/ocp/src/IO/newName.txt");

        File renameFile = new File("/Users/joshuataylor/git/java-class/ocp/src/IO/rename.txt");

        renameFile.renameTo(newName);

    }
}
