package SQLFun;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.SQLException;

public class ServerThreaded {

    private Socket socket = null;
    private ServerSocket server;

    public ServerThreaded(int port) throws IOException, SQLException {

        server= new ServerSocket(port);
        System.out.println("Server Started");

    }

    public void startServer() throws IOException{



        User user = null;
        String line = "";
        Thread newClient = null;

        while(true){

            socket = null;

            try {
                System.out.println("Waiting for a client..");

                socket = server.accept();
                System.out.println("socket = " + socket);

                ObjectInputStream inputStream = new ObjectInputStream(socket.getInputStream());

                newClient = new Thread(new WorkerThread(inputStream));
                newClient.start();

                System.out.println("Client Accepted");

            } finally {


            }



        }
    }


    public static void main(String[] args) throws IOException, SQLException,ClassNotFoundException {
        ServerThreaded server = new ServerThreaded(5000);
        server.startServer();
    }
}