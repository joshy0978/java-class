package SQLFun;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.SQLException;

public class Server {

    private Socket socket = null;
    private ServerSocket server = null;
    private ObjectInputStream inputStream = null;
    private UserJDBC database = null;

    public Server(int port) throws IOException, ClassNotFoundException, SQLException {

        server= new ServerSocket(port);
        System.out.println("Server Started");

        System.out.println("Waiting for a client..");

        socket = server.accept();
        System.out.println("Client Accepted");

        inputStream = new ObjectInputStream(socket.getInputStream());


        String dbUrl = "jdbc:sqlite:C:\\Users\\Student\\IdeaProjects\\Chapter1\\src\\SQLFun\\users.db";

        database = new UserJDBC(dbUrl);

        database.connect();

        User user = null;
        String line = "";

        while(!line.equals("Done")){

            user = (User) inputStream.readObject();
            System.out.println(user);
            line = user.userName;

            if (!line.equals("Done")) {
                if (user.requestType == RequestType.CREATE) {
                    System.out.println("adding to db");
                    database.addUser(user);
                }
                else if(user.requestType == RequestType.UPDATE){
                    System.out.println("updating to db");
                    database.updateuser(user);
                } else if (user.requestType == RequestType.DELETE) {
                    System.out.println("deleting DB entry");
                    database.deleteUser(user);
                }
            }

        }

        socket.close();
        server.close();
        database.close();
    }


    public static void main(String[] args) throws IOException, SQLException,ClassNotFoundException {
        Server server = new Server(5000);
    }
}
