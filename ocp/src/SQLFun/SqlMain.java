package SQLFun;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class SqlMain {

    public static void main(String[] args) throws SQLException {
        String url = "jdbc:sqlite:C:\\Users\\Student\\IdeaProjects\\Chapter1\\src\\SQLFun\\hubway.db";
        // create a connection to the database

        List<Station> stations = new ArrayList<>();
        List<JSONAble> jsonAbleList = new ArrayList<>();


        try(Connection conn = DriverManager.getConnection(url)){

            System.out.println("Connection to SQLite has been established.");
            Statement stmt = conn.createStatement();

            ResultSet rs = stmt.executeQuery("select * from stations");

            while(rs.next()) {
                Station station = new Station(rs);
                stations.add(station);
                jsonAbleList.add(station);
            }


        }

        stations.stream()
                .forEach(System.out::println);

        jsonAbleList.stream().forEach(
                (jsonAble)->{
                    System.out.println(jsonAble.convertToJson());
                }
        );


    }


}

