package SQLFun;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class UserJDBC {

    private String dbUrl;
    private Connection connection;

    public UserJDBC(String dbUrl){
        this.dbUrl = dbUrl;
    }

    public void connect() throws SQLException {
        this.connection = DriverManager.getConnection(dbUrl);
    }

    public void close() throws SQLException {
        this.connection.close();
    }

    public void addUser(User user) throws SQLException{

        //if no user table create user table in db
        this.createTable();

        Statement stmt = this.connection.createStatement();
        //INSERT INTO people(name, age) VALUES('Joe', 102);
        int result = stmt.executeUpdate("insert into users values( '" + user.userName + "','" + user.email + "')");
    }

    public void deleteUser(User user) throws SQLException {

        Statement stmt = this.connection.createStatement();
        //INSERT INTO people(name, age) VALUES('Joe', 102);
        int result = stmt.executeUpdate("delete from users where username == '" + user.userName + "'");
    }

    public void updateuser(User user) throws SQLException {

        Statement stmt = this.connection.createStatement();
        //INSERT INTO people(name, age) VALUES('Joe', 102);
        //int result = stmt.executeUpdate("update users set user ==" + user.userName);

        int result = stmt.executeUpdate("update users set email = '" + user.email + "' where username = '" + user.userName + "'");

    }

    private void createTable() throws SQLException {

        String sqlCreate = "CREATE TABLE IF NOT EXISTS users"
                + "  (username           VARCHAR(30),"
                + "   email            VARCHAR(30))";

        Statement stmt = this.connection.createStatement();
        stmt.execute(sqlCreate);
    }


    public static void main(String[] args) throws SQLException{

        String dbUrl = "jdbc:sqlite:/Users/joshuataylor/git/java-class/ocp/src/SQLFun/users.db";

        UserJDBC jdbc = new UserJDBC(dbUrl);

        jdbc.connect();

        //User user = new User("jt","jt@yahoo.com", RequestType.CREATE);
        User user2 = new User("kt","kt1234@yahoo.com", RequestType.CREATE);
//        User user3 = new User("lt","lt@yahoo.com", RequestType.CREATE);

//        jdbc.addUser(user);
//        jdbc.addUser(user2);
//        jdbc.addUser(user3);

        jdbc.updateuser(user2);
    }


}
