package SQLFun;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.Socket;
import java.sql.SQLException;

public class WorkerThread implements Runnable {


    private Socket socket = null;
    private UserJDBC database;
    private ObjectInputStream inputStream;


    public WorkerThread(ObjectInputStream inputStream) throws IOException{
        String dbUrl = "jdbc:sqlite:src/ClientServer/users.db";

        database = new UserJDBC(dbUrl);
        this.inputStream = inputStream;
        System.out.println("Worker Thread " + Thread.currentThread().getName() + " is started.");
    }

    private void closeConnections(){
        try {
            this.inputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void run() {

        System.out.println("Worker Thread " + Thread.currentThread().getName() + " is started.");
        try {
            database.connect();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        User user;
        String line = "";


        while(!line.equals("Done")){

            try {
                user = (User) inputStream.readObject();
               // user = new User("test", "e@yahoo.com", RequestType.CREATE);
                System.out.println(user);

                if (!user.userName.equals("Done")) {
                    if (user.requestType == RequestType.CREATE) {
                        System.out.println("adding to db");
                        database.addUser(user);
                    }
                    else if(user.requestType == RequestType.UPDATE){
                        System.out.println("updating to db");
                        database.updateuser(user);
                    } else if (user.requestType == RequestType.DELETE) {
                        System.out.println("deleting DB entry");
                        database.deleteUser(user);
                    }
                }
                else {
                    this.closeConnections();
                }

                break;
            } catch (IOException | SQLException | ClassNotFoundException e) {
                System.out.println("In work thread messing up " + e);
                break;
            }

        }

    }
}
