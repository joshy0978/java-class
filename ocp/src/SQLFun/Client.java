package SQLFun;

import java.io.*;
import java.net.Socket;
import java.sql.SQLOutput;

public class Client {

    private Socket socket = null;
    private BufferedReader input = null;
    private ObjectOutputStream output = null;

    public Client(String address, int port) throws IOException {

        //establish a connection
        socket = new Socket(address, port);
        System.out.println("Connected");

        input = new BufferedReader(new InputStreamReader(System.in));

        output = new ObjectOutputStream(socket.getOutputStream());


        String userName = "";
        String email = "";
        User user = null;
        String rqType = "";

        while(!userName.equals("Done") || !email.equals("Done")) {

            System.out.println("Please enter Username:");
            userName = input.readLine();
            System.out.println("Please enter Email:");
            email = input.readLine();
            System.out.println("RequestType: ");
            rqType = input.readLine();


            user = new User(userName, email, RequestType.valueOf(rqType));
            output.writeObject(user);
        }

        input.close();
        output.close();
        socket.close();

    }

    public static void main(String[] args) throws IOException{
        Client client = new Client("127.0.0.1", 5000);
    }
}
