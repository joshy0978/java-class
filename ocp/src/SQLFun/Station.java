package SQLFun;

import java.sql.ResultSet;
import java.sql.SQLException;

public class Station implements JSONAble{

    private int id;
    private String station;
    private String municipality;
    private int lat;
    private int lng;

    Station(ResultSet resultSet) throws SQLException {
        this.id = resultSet.getInt("id");
        this.station = resultSet.getString("station");
        this.municipality = resultSet.getString("municipality");
        this.lat = resultSet.getInt("lat");
        this.lng = resultSet.getInt("lng");
    }

    @Override
    public String convertToJson() {
        return "long: " + lng + " lat:"+ lat;
    }

    @Override
    public String toString() {
        return "Station in is id: " + id + " station name: " + station + " at the municipatlity " + municipality + " is located at lat: " + lat + " and long " + lng + ".";
    }
}
