package SQLFun;

import java.io.Serializable;

enum SearchResults {
    FOUND,
    NOTFOUND
}

enum RequestType {
    CREATE,
    SEARCH,
    FOUND,
    NOTFOUND,
    UPDATE,
    DELETE;

    public RequestType convertNumToRequestType(int i){
        switch (i) {
            case 1:
                return CREATE;
            case 2:
                return SEARCH;
            case 3:
                return FOUND;
            case 4:
                return NOTFOUND;
            case 5:
                return UPDATE;

                default:
                    return CREATE;
        }


    }
}

public class User implements Serializable {

    String userName;
    String email;
    RequestType requestType;

    public User(String userName, String email, RequestType requestType) {
        this.userName = userName;
        this.email = email;
        this.requestType = requestType;
    }

    public User(String userName) {
        this(userName,"empty", RequestType.CREATE);
    }


    @Override
    public String toString() {
        return "User is " + this.userName + " email: " + this.email;
    }
}
