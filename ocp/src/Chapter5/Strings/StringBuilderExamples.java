package Chapter5.Strings;

public class StringBuilderExamples {

    public static void main(String[] args) {

        //create a string builder object and save a reference to it
        StringBuilder b = new StringBuilder();

        //append(T t) has my overloaded types
        b.append(12345).append('-');

        //length()
        System.out.println(b.length());

        //indexOf(String s)
        System.out.println(b.indexOf("-"));

        //charAt(int index)
        System.out.println(b.charAt(2));

        //reverse()
        //this is a destructive operation
        StringBuilder b2 = b.reverse();

        //toString()
        System.out.println(b.toString());


        //testing to see if b points to b2
        System.out.println(b == b2);

        //insert(int offset, char c) and delete(int start, int end)
        b.insert(1, '-').delete(3, 4);

        System.out.println(b); //a-bde

        //substring(int start, int stop)
        System.out.println(b.substring(2, 4)); // bd


    }
}
