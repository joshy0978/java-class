package Chapter5.Strings;

public class Strings {

    public static void main(String[] args) {

        String s1 = "bunny";
        String s2 = "bunny";
        String s3 = new String("bunny");
        System.out.println(s1 == s2);
        System.out.println(s1 == s3);
        System.out.println(s1.equals(s3));


        String s4 = "1" + 2 + 3;
        String s5 = 1 + 2 + "3";
        System.out.println(s4); // 123
        System.out.println(s5); // 33

        // Common String Methods
        String s = "abcde ";

        //trim() and length()
        System.out.println(s.trim().length());

        //charAt()
        System.out.println(s.charAt(4));

        //indexOf(char c)
        System.out.println(s.indexOf('e'));

        //indexOf(String s)
        System.out.println(s.indexOf("de"));

        //subString(index start, index end) and toUpperCase()
        System.out.println(s.substring(2, 4).toUpperCase());

        //replace(char oldChar, char newChar)
        System.out.println(s.replace('a', '1'));

        //contains(String s)
        System.out.println(s.contains("DE"));

        //starts
        System.out.println(s.startsWith("a"));


    }
}
