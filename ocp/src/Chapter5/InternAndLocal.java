package Chapter5;

import java.util.Locale;

public class InternAndLocal {

    public static void main(String[] args) {


        Locale locale = Locale.getDefault();
        System.out.println(locale);

        System.out.println(Locale.GERMAN); // de
        System.out.println(Locale.GERMANY); // de_DE

        Locale l1 = new Locale.Builder() .setLanguage("en")
                .setRegion("US")
                .build();

        System.out.println(l1);


        Locale l2 = new Locale.Builder()
                .setRegion("US") .setLanguage("en") .build();

        System.out.println(l2);

        //changing the default
        System.out.println(Locale.getDefault()); // en_US
        Locale locale1 = new Locale("fr");
        Locale.setDefault(locale1);
        // change the default
        //don’t worry—the Locale changes only for that one Java program
        System.out.println(Locale.getDefault()); // fr


    }
}
