package Chapter5;

import java.util.Locale;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.Set;

public class UsingResourceBundles {

    public static void main(String[] args) {
        Locale us = new Locale("en", "US");
        Locale france = new Locale("fr", "FR");

        printProperties(us);
        System.out.println();
        printProperties(france);


        ResourceBundle rb_us = ResourceBundle.getBundle("Zoo", us);

        Properties props_us = new Properties();
        rb_us.keySet().stream()
                .forEach(k -> props_us.put(k, rb_us.getString(k)));

        System.out.println(props_us.getProperty("notReallyAProperty"));
        System.out.println(props_us.getProperty("notReallyAProperty", "123"));
        System.out.println(props_us.getProperty("hello", "no value found"));

        System.out.println();


        ResourceBundle rb_fr = ResourceBundle.getBundle("Zoo", france);

        Properties props_fr = new Properties();
        rb_us.keySet().stream()
                .forEach(k -> props_fr.put(k, rb_fr.getString(k)));

        System.out.println(props_fr.getProperty("notReallyAProperty"));
        System.out.println(props_fr.getProperty("notReallyAProperty", "123"));

    }


    public static void printProperties(Locale locale) {
        ResourceBundle rb = ResourceBundle.getBundle("Zoo", locale);
        System.out.println(rb.getString("hello"));
        System.out.println(rb.getString("open"));
        Set<String> keys = rb.keySet();
        keys.stream()
                .map(k -> k + " " + rb.getString(k))
                .forEach(System.out::println);
    }

}
