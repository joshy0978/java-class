package Chapter5;

import java.util.ListResourceBundle;
import java.util.Locale;
import java.util.ResourceBundle;

public class Tax_en_US extends ListResourceBundle {
    protected Object[][] getContents() {
        return new Object[][] { { "tax due date", new String("taxes are due april 15") } };
    }


    public static void main(String[] args) {
        ResourceBundle rb = ResourceBundle.getBundle( "Chapter5.Tax", Locale.US);
        System.out.println(rb.getObject("tax due date")); }

}
