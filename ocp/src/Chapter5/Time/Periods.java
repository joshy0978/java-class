package Chapter5.Time;

import java.time.LocalDate;
import java.time.Period;

public class Periods {

    public static void main(String[] args) {


        /******************
         *
         * The Period class uses the units year, month and day to represent a period of time.
         *
         * We can obtain a Period object as the difference between two dates by using the between() method:
         */

        LocalDate startDate = LocalDate.of(2015, 2, 20);
        LocalDate endDate = LocalDate.of(2017, 1, 15);

        Period period = Period.between(startDate, endDate);


        /****************
         *
         * the isNegative() method, which returns true if any of the units are negative, can be used to determine if the endDate is higher than the startDate:
         *
         */

        System.out.println(period.isNegative());


        /**************
         *
         * Another way to create a Period object is based on the number of days, months, weeks or years using dedicated methods:
         *
         */

        Period fromUnits = Period.of(3, 10, 10);
        System.out.println("Period from units: " + fromUnits);

        Period fromDays = Period.ofDays(50);
        System.out.println("Period from units: " + fromDays);

        Period fromMonths = Period.ofMonths(5);
        System.out.println("Period from units: " + fromMonths);

        Period fromYears = Period.ofYears(10);
        System.out.println("Period from units: " + fromYears);

        Period fromWeeks = Period.ofWeeks(40);
        System.out.println("Period from units: " + fromWeeks);


        /********************
         *
         * We can also create a Period object by parsing a text sequence, which has to have the format “PnYnMnD”:
         *
         */

        Period fromCharYears = Period.parse("P2Y");
        System.out.println("Period from Char Years: " + fromCharYears);

        Period fromCharUnits = Period.parse("P2Y3M5D");
        System.out.println("Period from Char Units: " + fromCharUnits);

    }
}
