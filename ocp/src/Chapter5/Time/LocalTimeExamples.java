package Chapter5.Time;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public class LocalTimeExamples {

    public static void main(String[] args) {



        LocalTime localTime1 = LocalTime.of(9, 10, 50);
        LocalTime localTime2 = LocalTime.of(9, 10, 50);
        LocalTime localTime3 = LocalTime.of(11, 45, 20);

        // compareTo() example
        if (localTime1.compareTo(localTime2) == 0) {
            System.out.println("localDate1 and localDate2 are equal");
        } else {
            System.out.println("localDate1 and localDate2 are not equal");
        }

        // isBefore() example
        if (localTime2.isBefore(localTime3)) {
            System.out.println("localTime2 comes before localTime3");
        }

        // isAfter() example
        if (localTime3.isAfter(localTime1)) {
            System.out.println("localTime3 comes after localTime1");
        }



        LocalTime localTime = LocalTime.now();

        // ISO Format
        DateTimeFormatter timeFormatter = DateTimeFormatter.ISO_LOCAL_TIME;
        System.out.println(localTime.format(timeFormatter));

        // hour-of-day (0-23)
        DateTimeFormatter timeFormatter1 = DateTimeFormatter
                .ofPattern("HH:mm:ss");
        System.out.println(localTime.format(timeFormatter1));

        // clock-hour-of-am-pm (1-24)
        DateTimeFormatter timeFormatter2 = DateTimeFormatter
                .ofPattern("kk:mm:ss");
        System.out.println(localTime.format(timeFormatter2));

        // clock-hour-of-am-pm (1-12)
        DateTimeFormatter timeFormatter3 = DateTimeFormatter
                .ofPattern("hh:mm:ss a");
        System.out.println(localTime.format(timeFormatter3));

        // hour-of-am-pm (0-11)
        DateTimeFormatter timeFormatter4 = DateTimeFormatter
                .ofPattern("KK:mm:ss a");
        System.out.println(localTime.format(timeFormatter4));

    }



}
