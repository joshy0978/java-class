package Chapter5.Time;

import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;

class DurationTest {

    public static void main(String[] args) {



        Duration daily = Duration.ofDays(1);
        System.out.println("Daily Duration: " + daily);

        Duration hourly = Duration.ofHours(1);
        System.out.println("Hourly Duration: " + hourly);

        Duration everyMinute = Duration.ofMinutes(1);
        System.out.println("One Minute Duration: " + everyMinute);

        Duration everyTenSeconds = Duration.ofSeconds(10);
        System.out.println("Every Ten Seconds Duration: " + everyTenSeconds);

        Duration everyMilli = Duration.ofMillis(1);
        System.out.println("Every Milli Duration: " + everyMilli);

        Duration everyNano = Duration.ofNanos(1);
        System.out.println("Every Nano Duration: " + everyNano);



        Duration dailyChron = Duration.of(1, ChronoUnit.DAYS);
        System.out.println("Daily Chron: " + dailyChron);

        Duration hourlyChron = Duration.of(1, ChronoUnit.HOURS);
        System.out.println("Daily Duration: " + hourlyChron);

        Duration everyMinuteChron = Duration.of(1, ChronoUnit.MINUTES);
        System.out.println("Daily Duration: " + everyMinuteChron);

        Duration everyTenSecondsChron = Duration.of(10, ChronoUnit.SECONDS);
        System.out.println("Daily Duration: " + everyTenSecondsChron);

        Duration everyMilliChron = Duration.of(1, ChronoUnit.MILLIS);
        System.out.println("Daily Duration: " + everyMilliChron);

        Duration everyNanoChron = Duration.of(1, ChronoUnit.NANOS);
        System.out.println("Daily Duration: " + everyNanoChron);

        Duration fromChar1 = Duration.parse("P1DT1H10M10.5S");
        System.out.println("Duration FromChar1: " + fromChar1);

        Duration fromChar2 = Duration.parse("PT10M");
        System.out.println("Duration FromChar2:" + fromChar2);



    }
}
