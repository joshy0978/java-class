package Chapter5.Time;

import java.time.LocalDate;

enum TICKET_TIME {

    ONE_DAY,
    THREE_DAY,
    FIVE_DAY

}

public class BusTicket {

    private LocalDate expireDate;

    public BusTicket(TICKET_TIME ticket_time) {
        switch (ticket_time){
            case ONE_DAY:
                expireDate = LocalDate.now().plusDays(1);
                break;
            case FIVE_DAY:
                expireDate = LocalDate.now().plusDays(5);
                break;
            case THREE_DAY:
                expireDate = LocalDate.now().plusDays(3);
                break;

                default:
                    expireDate = LocalDate.now();

        }

    }

    boolean isTicketValid(){

        if(LocalDate.now().isBefore(expireDate)) {
            return true;
        }
        return false;

    }

    public static void main(String[] args) {

        BusTicket issuedTicketOneDay = new BusTicket(TICKET_TIME.ONE_DAY);
        BusTicket issuedTicketThreeDay = new BusTicket(TICKET_TIME.THREE_DAY);
        BusTicket issuedTicketFiveDay = new BusTicket(TICKET_TIME.FIVE_DAY);

        System.out.println(issuedTicketOneDay.isTicketValid());
        System.out.println(issuedTicketThreeDay.isTicketValid());
        System.out.println(issuedTicketFiveDay.isTicketValid());

    }
}
