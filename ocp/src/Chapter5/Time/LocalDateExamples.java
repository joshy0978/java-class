package Chapter5.Time;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.Month;

public class LocalDateExamples {

    public static void main(String[] args) {


        LocalDate localDate = LocalDate.now();
        System.out.println("Local Date using now");
        System.out.println(localDate);

        System.out.println("\nLocal Date using of()");
        LocalDate localDate2 = LocalDate.of(2015, 12, 31);
        System.out.println(localDate2);


        //Using Month Enums
        System.out.println("\nLocal Date using of() with month enum");
        LocalDate localDateEnum = LocalDate.of(2017, Month.AUGUST, 21);
        System.out.println(localDateEnum);

        /*****************
         *
         * You can access the date information of a LocalDate using these methods:
         *
         * getYear()
         * getMonth()
         * getDayOfMonth()
         * getDayOfWeek()
         * getDayOfYear()
         *
         */

        int   year       = localDate.getYear();
        System.out.println("\nyear: " + year);


        Month month      = localDate.getMonth();
        System.out.println("\nmonth: " + month);

        int   dayOfMonth = localDate.getDayOfMonth();
        System.out.println("\ndayOfMonth: " + dayOfMonth);

        int   dayOfYear  = localDate.getDayOfYear();
        System.out.println("\ndayOfYear");

        DayOfWeek dayOfWeek = localDate.getDayOfWeek();
        System.out.println("\ndayOfWeek");

        /******************
         * You can perform a set of simple date calculations with the LocalDate class
         * using one or more of the following methods:
         *
         * plusDays()
         * plusWeeks()
         * plusMonths()
         * plusYears()
         * minusDays()
         * minusWeeks()
         * minusMonths()
         * minusYears()
         */

        LocalDate localDate3 = LocalDate.of(2015, 12, 31);
        System.out.println("\nlocalDate3 " + localDate3);

        System.out.println("\nlocalDate3 + 3 years: " + localDate.plusYears(3));

        System.out.println("\nlocalDate3 + 3 Days: " + localDate.plusDays(3));

        System.out.println("\nlocalDate3 + 3 Months: " + localDate.plusMonths(3));

        System.out.println("\nlocalDate3 + 3 weeks: " + localDate.plusWeeks(3));

        System.out.println("\nlocalDate3 + minus 3 years: " + localDate.minusYears(3));

        System.out.println("\nlocalDate3 + minus 3 Days: " + localDate.minusDays(3));

        System.out.println("\nlocalDate3 + minus 3 weeks: " + localDate.minusWeeks(3));


    }

}
