package Chapter5.Time;

import java.time.*;

class Instances {

    public static void main(String[] args) {

        /*******************
         * Creating an Instant
         * You create an Instant instance using one of the Instant class factory methods.
         * For instance, to create an Instant which represents this exact moment of now, call Instant.now(), like this:
         */

        Instant now = Instant.now();
        System.out.println("now: " + now);

        LocalDate date = LocalDate.of(2015, 5, 25);
        LocalTime time = LocalTime.of(11, 55, 00);
        ZoneId zone = ZoneId.of("US/Eastern");
        ZonedDateTime zonedDateTime = ZonedDateTime.of(date, time, zone);
        Instant instant = zonedDateTime.toInstant();
        System.out.println(instant);

    }

}
