package Threads;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

public class MemConsit {

    public static void main(String[] args) {

        Map<String, Object> foodData = new HashMap<String, Object>();
        foodData.put("penguin", 1);
        foodData.put("flamingo", 2);
        for(String key: foodData.keySet())
            foodData.remove(key);

//
//        Map<String, Object> foodData1 = new ConcurrentHashMap<String, Object>();
//        foodData1.put("penguin", 1);
//        foodData1.put("flamingo", 2);
//        for(String key: foodData1.keySet())
//            foodData1.remove(key);



    }

}

class CopyOnWriteExample {

    public static void main(String[] args) {

        CopyOnWriteArrayList<Integer> numbers
                = new CopyOnWriteArrayList<>(new Integer[]{1, 3, 5, 8});


        Iterator<Integer> iterator = numbers.iterator();

        //Keep in mind that, when we create an iterator for the CopyOnWriteArrayList,
        // we get an immutable snapshot of the data in the list at the time iterator() was called.
        numbers.add(10);

        iterator.forEachRemaining(System.out::println);


        //Removing While Iterating is Not Allowed

        /*********
         *
         * The CopyOnWriteArrayList was created to allow for the
         * possibility of safe iterating over elements even when the underlying list gets modified.
         *
         * Because of the copying mechanism, the remove() operation on the returned Iterator is not
         * permitted – resulting with UnsupportedOperationException:
         *
         */

        iterator = numbers.iterator();
        while (iterator.hasNext()) {
            iterator.remove();
        }

    }


}
