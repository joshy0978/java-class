package Threads.ExecutorServiceExamples;

import java.util.Date;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

class ScheduleTask implements Runnable
{
    private String name;

    public ScheduleTask(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public void run()
    {
        try {
            System.out.println("Doing a task during : " + name + " - Time - " + new Date());
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}

class ScheduledThreadPoolExecutorExample
{
    public static void main(String[] args)
    {
        ScheduledExecutorService executor = Executors.newScheduledThreadPool(2);
        ScheduleTask task1 = new ScheduleTask("Demo Task 1");
        ScheduleTask task2 = new ScheduleTask("Demo Task 2");
        ScheduleTask task3 = new ScheduleTask("Demo Task3");

        System.out.println("The time is : " + new Date());

        executor.schedule(task1, 5 , TimeUnit.SECONDS);
        executor.schedule(task2, 10 , TimeUnit.SECONDS);
        executor.scheduleAtFixedRate(task3, 5, 10, TimeUnit.SECONDS);



        try {
            executor.awaitTermination(1, TimeUnit.MINUTES);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


        executor.shutdown();

    }
}
