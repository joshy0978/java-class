package Threads;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

public class ExecutorServiceMethods {

    public static void main(String[] args) {


        ExecutorService singleThread = Executors.newSingleThreadExecutor();

        singleThread.execute(() -> {
            System.out.println("Work on thread");
        });

        Callable<Integer> callableTask = ()->{
            System.out.println("Multiplying on thread");
            return 345*2345;
        };

        /***********
         *
         * submit() submits a Callable or a Runnable task to an ExecutorService and returns a result of type Future.
         *
         */
        Future<Integer> returnFuture = singleThread.submit(callableTask);

        List<Callable<Integer>> listOfTasks = new ArrayList<>();

        listOfTasks.add(()-> {
            System.out.println("Multiplying on thread task 1");
            return 5 * 2345;
        });

        listOfTasks.add(()-> {
            System.out.println("Multiplying on thread task 2");
            return 5 * 2;
        });

        listOfTasks.add(()-> {
            System.out.println("Multiplying on thread task 3");
            return 65 * 245;
        });

        /**********
         *
         * invokeAll() assigns a collection of tasks to an ExecutorService, causing each to be executed,
         * and returns the result of all task executions in the form of a list of objects of type Future.
         *
         */

        List<Future<Integer>> futureList = new ArrayList<>();
        try{
            futureList = singleThread.invokeAll(listOfTasks);
        } catch (InterruptedException e) {
            System.out.println("InterruptedException caught: " + e.getMessage());
        }

        for (Future<Integer> future: futureList) {
            try{
                while(true){
                    if (future.isDone()){
                        System.out.println("Future value: " +future.get());
                        break;
                    }else {
                        future.cancel(true);
                        System.out.println("Future is not done");
                    }
                }
            } catch (InterruptedException | ExecutionException e) {
                System.out.println("Exception getting future: " + e.getMessage());
            }
        }


        /**********
         *
         *invokeAny() assigns a collection of tasks to an ExecutorService, causing each to be executed,
         * and returns the result of a successful execution of one task (if there was a successful execution).
         *
         */

        Integer invokeAnyValue = 0;
        try{
            invokeAnyValue = singleThread.invokeAny(listOfTasks);
        } catch (InterruptedException | ExecutionException e) {
            System.out.println("InterruptedException caught: " + e.getMessage());
        }

        System.out.println("invkeAnyValue value: " + invokeAnyValue);




    }

}
