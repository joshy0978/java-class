package Threads;


//Extending Thread
class ThreadDemo extends Thread {
    @Override
    public void run() {
        // Some code here...

    }
}

//Implementig Runnable
class RunnableDemo implements Runnable {
    @Override
    public void run() {
        // Some code here...
    }
}

public class CreatingThreads {

    public static void main(String[] args) {
        // Way 1
        new ThreadDemo().start();

        //Way 2
        new Thread(new RunnableDemo()).start();
    }
}
