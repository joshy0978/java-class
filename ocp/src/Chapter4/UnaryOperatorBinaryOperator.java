package Chapter4;

import java.util.function.BinaryOperator;
import java.util.function.UnaryOperator;

/***************
 * UnaryOperator is used to work on a single operand. It returns the same type as an operand.
 *
 * T apply(T t)
 *
 * Binary accepts two operands of the same type and process it and then returns results of the same type as operands.
 *
 * T apply(T t1, T t2)
 */

class ScaleIntegerBy5 implements UnaryOperator<Integer> {
    @Override
    public Integer apply(Integer integer) {
        return integer * 5;
    }
}

class ScaleInteger implements BinaryOperator<Integer> {
    @Override
    public Integer apply(Integer integer, Integer integer2) {
        return integer * integer2;
    }
}

public class UnaryOperatorBinaryOperator {

    public static void main(String[] args) {

        System.out.println("\nUnaryOperator Examples:");

        //UnaryOperator Class
        ScaleIntegerBy5 scaleIntegerBy5 = new ScaleIntegerBy5();
        System.out.println(scaleIntegerBy5.apply(2));


        //UnaryOperator Anon Class
        UnaryOperator<Integer> scaleBy5Anon = new UnaryOperator<Integer>() {
            @Override
            public Integer apply(Integer integer) {
                return integer * 5;
            }
        };

        System.out.println(scaleBy5Anon.apply(7));

        UnaryOperator<Integer> scaleUnaryLambda = i -> i * 10;
        System.out.println(scaleUnaryLambda.apply(99));

        System.out.println("\nBinaryOperator Examples:");
        //BinaryOperator Class
        ScaleInteger scaleInteger = new ScaleInteger();
        System.out.println(scaleInteger.apply(5 ,4));

        //Binary Anon Class
        BinaryOperator<Integer> binaryOperatorScaler = new BinaryOperator<Integer>() {
            @Override
            public Integer apply(Integer integer, Integer integer2) {
                return integer * integer2;
            }
        };

        System.out.println( binaryOperatorScaler.apply(6,4));

        BinaryOperator<Integer> binaryOperatorLambda = (i, t) -> i * t;
        System.out.println(binaryOperatorLambda.apply(3,1));




    }



}
