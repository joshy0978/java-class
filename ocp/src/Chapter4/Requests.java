package Chapter4;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Supplier;

class FindValidRequest implements Supplier<Integer> {

    Requests requests;

    FindValidRequest(Requests r) {
        requests = r;
    }

    @Override
    public Integer get() {
        System.out.println("Inside supplier lambda");
        int i = 4;
        Optional<Integer> requestValue = requests.getRequest(4);
        while(!requestValue.isPresent()) {
            ++i;
            requestValue = requests.getRequest(i);
        }
        return requestValue.get();
    }
}

public class Requests {

    Map<Integer, Integer> requestsIndexMap = new HashMap<>();

    Requests(){
        requestsIndexMap.put(1, 1);
        requestsIndexMap.put(2,2);
        requestsIndexMap.put(7,7);
        requestsIndexMap.put(10,10);
    }

    public Optional<Integer> getRequest(int index) {

        Integer returnValue = requestsIndexMap.get(index);

        if (returnValue == null) {
            return Optional.empty();
        }

        return Optional.of(returnValue);

    }

    public static void main(String[] args) {

        Requests currentRequests = new Requests();

        Optional<Integer> requestFor1 = currentRequests.getRequest(1);

        Optional<Integer> requestFor3 = currentRequests.getRequest(3);

        System.out.println(requestFor1.get());


        Integer request3orElse = requestFor3.orElse(-1);

        System.out.println("request3orElse: " + request3orElse);


        Supplier<Integer> supplier = ()-> {
            System.out.println("Inside supplier lambda");
            int i = 4;
            Optional<Integer> requestValue = currentRequests.getRequest(4);
            while(!requestValue.isPresent()) {
                ++i;
                requestValue = currentRequests.getRequest(i);
            }
            return requestValue.get();
        };

        Integer optionalIntValue = requestFor3.orElseGet(supplier);

        goGetIndexFromTable(requestFor3.orElseGet(supplier));

        System.out.println("Optional int: " + optionalIntValue);



        if(requestFor3.isPresent()) {
            goGetIndexFromTable(requestFor3.get());
            System.out.println(requestFor3.get());
        } else {
            goGetIndexFromTable(-1);
        }

        goGetIndexFromTable(requestFor3.orElse(-99999));


        Integer optionalIntValueForRequest1 = requestFor1.orElseGet(supplier);

        System.out.println("Optional int for request 1: " + optionalIntValueForRequest1);


        /*************
         * supplier class
         */
        Supplier<Integer> findValidRequestSupplier = new FindValidRequest(currentRequests);

        int valueFromClassSupplier = requestFor3.orElseGet(findValidRequestSupplier);

        System.out.println("valueFromClassSupplier: " + valueFromClassSupplier);


    }

    static void goGetIndexFromTable(int indexValue) {
        if (indexValue == -99999) {
            //log invalid requests
            return;
        }

        //Do work


    }
}
