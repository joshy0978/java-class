package Chapter4;

/******************
 * The Supplier interface signature is as below, which represents a supplier of results.
 *
 * @FunctionalInterface
 *
 * public interface Supplier
 *
 * Here, T is the type of results supplied by this supplier.
 *
 * T get():This abstract method does not accept any argument but
 *         instead returns newly generated values, T, in the stream.
 *         But there is no requirement that new or distinct results
 *         be returned each time the supplier is invoked.
 */

import java.util.function.Supplier;

class CustomClass {}

class CustomClassSupplier implements Supplier<CustomClass> {
    @Override
    public CustomClass get() {
        return new CustomClass();
    }
}

public class SupplierExample {

    public static void main(String[] args) {

        //Using a class that implements Supplier
        Supplier<CustomClass> customClassSupplier = new CustomClassSupplier();

        CustomClass newObject = SupplierExample.customClassFactory(customClassSupplier);

        //Using anonymous class
        Supplier<CustomClass> customClassSupplierAnon = new Supplier<CustomClass>() {
            @Override
            public CustomClass get() {
                return new CustomClass();
            }
        };

        newObject = SupplierExample.customClassFactory(customClassSupplierAnon);

        //Using Lambda
        newObject = SupplierExample.customClassFactory(() -> new CustomClass());

        //Using Method Reference
        newObject = SupplierExample.customClassFactory(CustomClass::new);

    }

    public static CustomClass customClassFactory(Supplier<CustomClass> supplier) {
        return supplier.get();
    }
}
