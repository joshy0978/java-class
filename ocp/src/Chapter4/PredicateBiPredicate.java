package Chapter4;

import java.util.function.BiPredicate;
import java.util.function.Predicate;

/*****************
 * The Predicate interface represents an operation that takes a single input and returns a boolean value.
 *
 * boolean test(T t)
 */

class CustomPred implements Predicate<String> {
    @Override
    public boolean test(String s) {
        return s.equals("Test");
    }
}

class CustomBiPred implements BiPredicate<String, String> {
    @Override
    public boolean test(String s, String s2) {
        return s.equals(s2);
    }
}

public class PredicateBiPredicate {

    public static void main(String[] args) {


        //Custom Class Pred
        CustomPred customPred = new CustomPred();
        System.out.println(customPred.test("Test"));

        //Anon Pred
        Predicate<String> anonPred = new Predicate<String>() {
            @Override
            public boolean test(String s) {
                return s.equals("Test");
            }
        };
        System.out.println(anonPred.test("Test"));

        //Lambda Pred
        Predicate<String> lambdaPred = (s) -> {return s.equals("Test");};
        System.out.println(lambdaPred.test("Later"));


        //Custom Class BiPred
        CustomBiPred customBiPred = new CustomBiPred();
        System.out.println(" Custom BiPred " + customBiPred.test("Test", "Test"));


        //Anon BiPred
        BiPredicate<String, String> biPredicate = new BiPredicate<String, String>() {
            @Override
            public boolean test(String s, String s2) {
                return s.equals(s2);
            }
        };

        System.out.println("biPredicate Anon: " + biPredicate.test("Hello", "Hey"));

        //Anon Lambda
        BiPredicate<String, String> biPredicateLambda = (s1, s2) -> {return s1.equals(s2);};
        System.out.println("biPrecicateLambda: " + biPredicateLambda.test("Hi", "Hello"));





    }
}
