package Chapter4;

import java.util.function.BiFunction;
import java.util.function.Function;

/*******************
 * A Function is a functional interface whose sole purpose is to return any
 * result by working on a single input argument. It accepts an argument of
 * type T and returns a result of type R, by applying specified logic on the input via the apply method.
 *
 * R apply(T t)
 */

class CentigradeToFahrenheitInt implements Function<Integer, Double> {
    @Override
    public Double apply(Integer integer) {
        return new Double((integer*9/5)+32);
    }
}

class CombineIntegerAndString implements BiFunction<Integer, String, String> {
    @Override
    public String apply(Integer integer, String s) {
        return s + integer.toString();
    }
}

public class FunctionBiFunction {

    public static void main(String[] args) {

        //Function Class
        CentigradeToFahrenheitInt centigradeToFahrenheitInt = new CentigradeToFahrenheitInt();
        System.out.println(centigradeToFahrenheitInt.apply(10));

        //Function Anon Class
        Function<Integer, Double> centigradeToFahrenheitAnon = new Function<Integer, Double>() {
            @Override
            public Double apply(Integer integer) {
                return new Double((integer*9/5)+32);
            }
        };
        System.out.println(centigradeToFahrenheitAnon.apply(32));

        //Function Lambda
        Function<Integer, Double> centigradeToFahrenheitLambda = (temp) -> {return new Double((temp*9/5)+32);};
        System.out.println(centigradeToFahrenheitLambda.apply(50));

        //Bifuction Class
        CombineIntegerAndString combineIntegerAndString = new CombineIntegerAndString();
        System.out.println(combineIntegerAndString.apply(50, "The integer entered is: "));

        //BiFunction Anon Class
        BiFunction<Integer, String, String> biFunctionAnon = new BiFunction<Integer, String, String>() {
            @Override
            public String apply(Integer integer, String s) {
                return s + integer.toString();
            }
        };
        System.out.println(biFunctionAnon.apply(24, "The integer entered is: "));

        //BiFunction Lambda
        BiFunction<Integer, String, String> biFunctionLambda = (i, s) -> {return s + i.toString(); };
        System.out.println(biFunctionLambda.apply(10, "The integer entered is: "));
    }
}
