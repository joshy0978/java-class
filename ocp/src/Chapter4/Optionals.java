package Chapter4;

import java.util.Optional;

/*********************
 *Optionals are
 *
 * Optional.of()
 * creates an optional with that value
 *
 * Optional.empty()
 * method is useful to create an empty Optional object.
 */

class OptionalBasicExample {

    public static void main(String[] args) {

        Optional<String> gender = Optional.of("MALE");
        Optional<String> name = Optional.empty();

        System.out.println("gender Optional:" + gender);
        System.out.println("gender Optional: Gender value : " + gender.get());
        System.out.println("Empty Optional: " + Optional.empty());

        //This Line will throw a NoSuchElementException: No Value present exception
        //System.out.println("gender Optional: Gender value : " + name.get());

        //Proper way to check if an optional has a value
        if(name.isPresent()) {
            System.out.println("Value of name: " + name.get());
        } else {
            System.out.println("Value of name: Null");
        }


        //orElse returns Optional value if nonNull
        //or returns what is passed in orElse method
        String orElseValue = name.orElse(stringFactory());
        System.out.println(orElseValue);
        System.out.println(name.orElseGet(()->{ return "This is created in orElseGet";}));

        Optional<Integer> integerOptional = Optional.of(44);

        System.out.println(integerOptional.orElseGet(()->new Integer(99)));

        integerOptional.ifPresent(s -> System.out.println( s + " is the value"));





    }


    static String stringFactory() {
        return "String Factory";
    }
}
