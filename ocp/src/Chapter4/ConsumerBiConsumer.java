package Chapter4;

import org.omg.PortableInterceptor.INACTIVE;

import java.util.function.BiConsumer;
import java.util.function.Consumer;

/********************
 * the Consumer interface accepts any type of object as input.
 * The java.util.function.Consumer class has one non-default
 * method named accept which takes a single object as its argument and has a void return type.
 *
 * Consumer:
 *  accept(T t)
 *
 *  BiConsumer:
 *  accept(T t, U u)
 */


class CustomConsumer implements Consumer<String> {
    @Override
    public void accept(String s) {
        System.out.println("Printing value: " + s + " from class consumer");
    }
}

class CustomBiConsumer implements BiConsumer<Integer, Integer> {
    @Override
    public void accept(Integer integer, Integer integer2) {
        System.out.println(integer.toString() + " + " + integer2.toString() + " = " + (integer + integer2));
    }
}

public class ConsumerBiConsumer {

    public static void main(String[] args) {

        //Consumer Custom Class
        CustomConsumer classConsumer = new CustomConsumer();
        classConsumer.accept("Hello World");

        //Consumer Anon Class
        Consumer<String> anonConsumer = new Consumer<String>() {
            @Override
            public void accept(String s) {
                System.out.println("Printing value: " + s + " from anon consumer");
            }
        };
        anonConsumer.accept("Hello World");
        //Consumer Lambda
        Consumer<String> lambdaConsumer = (s) -> System.out.println("Printing value: " + s + " from lamba consumer");
        lambdaConsumer.accept("Hello World");

        //BiConsumer Custom Class
        CustomBiConsumer customBiConsumer = new CustomBiConsumer();
        customBiConsumer.accept(5, 4);

        //BiConsumer Anon Class
        BiConsumer<Integer, Integer> anonBiConsumer = new BiConsumer<Integer, Integer>() {
            @Override
            public void accept(Integer integer, Integer integer2) {
                System.out.println(integer.toString() + " + " + integer2.toString() + " = " + (integer + integer2));
            }
        };
        anonBiConsumer.accept(26,4);

        //BiConsumer Lambda
        BiConsumer<Integer, Integer> lambdaBiConsumer = (i1, i2) -> System.out.println(i1.toString() + " + " + i2.toString() + " = " + (i1 + i2));
        lambdaBiConsumer.accept(10, 20);

    }
}
