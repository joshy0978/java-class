package Chapter4;

import java.util.function.Consumer;
import java.util.function.Supplier;

public final class CustomOptional<T> {

    final private boolean isValidValue;
    final private T value;

    private CustomOptional(T s, boolean valid) {
        value = s;
        isValidValue = valid;
    }

    public static <T> CustomOptional<T> empty(){
        return new CustomOptional<>(null, false);
    }

    public static <T> CustomOptional<T> of(T s){
        return new CustomOptional<>(s, true);
    }

    public T get(){
        if(isPresent()){
            return value;
        }
        throw new RuntimeException();
    }

    void ifPresent(Consumer<T> c){
        if (isValidValue) {
            c.accept(value);
        }
    }

    boolean isPresent() {
        return isValidValue;
    }

    T orElse(T other) {
        //     boolean        true    false
        return isValidValue ? value : other;

    }

    T orElseGet(Supplier<T> s){
        return isValidValue ? value : s.get();
    }


}
