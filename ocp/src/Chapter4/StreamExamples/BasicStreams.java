package Chapter4.StreamExamples;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

class Employee {
    int employeeNumber;
    String name;
    double salary;

    public Employee(int employeeNumber, String name, double salary) {
        this.employeeNumber = employeeNumber;
        this.name = name;
        this.salary = salary;
    }

    public void salaryIncrement(double increaseAmout) {
        salary += increaseAmout;
    }

    public double getSalary() {
        return salary;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return employeeNumber;
    }

    @Override
    public String toString() {
        return name + " is employee number: " + employeeNumber + " and makes a salary of: " + salary;
    }
}

public class BasicStreams {

    public static void main(String[] args) {

        Employee[] arrayOfEmps = {
                new Employee(1, "Jeff Bezos", 100000.0),
                new Employee(2, "Bill Gates", 200000.0),
                new Employee(3, "Mark Zuckerberg", 300000.0)
        };

        System.out.println("\nGetting a stream:");

        //Get a stream from
        Stream.of(arrayOfEmps);

        //Convert an array to List
        List<Employee> empList = Arrays.asList(arrayOfEmps);
        //Get stream of empList aka source
        Stream<Employee> streamOfList = empList.stream();

        System.out.println("\n");
        System.out.println("***********************");
        System.out.println("Operations");
        System.out.println("***********************");

        //***********************
        //Operations
        //***********************

        System.out.println("\nFilter operation\n");
        //Filter (Intermediate Operation)

        List<String> strings = Arrays.asList("abc", "", "bc", "efg", "abcd","", "jkl");
        List<String> filtered = strings.stream().filter(string -> !string.isEmpty()).collect(Collectors.toList());
        System.out.println("Filtered list: " + filtered);

        System.out.println("\nFilter without Streams\n");
        //Filter without Stream

        List<String> filteredWithoutStream = new ArrayList<>();
        for (String s : strings) {
            if (!s.isEmpty()) {
                filteredWithoutStream.add(s);
            }
        }
        System.out.println("Filter without stream: " + filteredWithoutStream);

        streamOfList.forEach(System.out::println);

        System.out.println("\nFor Each operation\n");
         //for Each (Terminal operation)

        //Streams are one time use only. Need to get another stream
        streamOfList = empList.stream();
        streamOfList.forEach(e -> e.salaryIncrement(1000.00));

        streamOfList = empList.stream();
        streamOfList.forEach(System.out::println);

        for (int i = 0; i < empList.size(); i++) {
            Employee e = empList.get(i);
            e.salaryIncrement(5000);
        }

        System.out.println("Emplist: " + empList);

        System.out.println("\nMap operation\n");
        // Map
        // map() produces a new stream after applying a function to each element of the original stream.
        // The new stream could be of different type.
        Integer[] empIds = {1, 2, 3};

        List<Employee> employeesFromId = Stream.of(empIds)
                .map(i -> empList.get(i- 1))
        .collect(Collectors.toList());

        System.out.println("List of employees by id: " + employeesFromId);

        //Non Stream Version
        employeesFromId.clear();
        for (Integer i: empIds) {
            employeesFromId.add(empList.get(i - 1));
        }

        System.out.println("List of employees by id: " + employeesFromId);


        System.out.println("\nFind First operation\n");
        //Find First (Terminating Operation)
        //findFirst() returns an Optional for the first entry in the stream; the Optional can, of course, be empty
        Employee employee = Stream.of(empIds)
                .map(i -> empList.get(i- 1))
                .filter(e -> e != null)
                .filter(e -> e.getSalary() > 100000)
                .findFirst()
                .orElse(null);

        System.out.println("Find First Employee: " + employee);

        //Without Find First
        Employee employee1 = null;

        for (Integer i : empIds  ) {
            Employee e = empList.get(i - 1);
            if(e != null && e.getSalary() > 100000) {
                employee1 = e;
                break;
            }
        }
        if(employee1 != null) {
            System.out.println("Employee1: " + employee1);

        }

        System.out.println("\ntoArray operation\n");
        //ToArray (Terminating Operation)
        Employee[] employees = empList.stream().toArray(Employee[]::new);
        Stream.of(employees).forEach(System.out::println);


        System.out.println("\nPeak operation\n");
        //Peak
        empList.stream()
                .peek(e -> e.salaryIncrement(10.0))
                .peek(System.out::println)
                .collect(Collectors.toList());

        System.out.println("\ncount operation\n");
        //Count
        Long empCount = empList.stream()
                .filter(e -> e.getSalary() > 200000)
                .count();

        System.out.println("empCount " +empCount);

        System.out.println("\nInfinite operation\n");
        //Infinite Stream
        Stream<Integer> infiniteStream = Stream.iterate(2, i -> i * 2);

        List<Integer> collect = infiniteStream
                .skip(3)
                .limit(5)
                .collect(Collectors.toList());

        System.out.println("InfiniteStream result: " + collect + "\n");


        List<Double> rand_nums = Stream.generate(Math::random)
                .limit(5)
                .collect(Collectors.toList());

        rand_nums.forEach(System.out::println);

        System.out.println("\nSorting operation\n");
        //Sorting
        List<Employee> sortedEmployees = empList.stream()
                .sorted((e1, e2) -> e1.getName().compareTo(e2.getName()))
                .collect(Collectors.toList());

        System.out.println("Sorted Employees: " + sortedEmployees);


        System.out.println("\nmin and max operation\n");
        // min and max
        // As the name suggests, min() and max() return the minimum and maximum element in the stream respectively,
        // based on a comparator.
        // They return an Optional since a result may or may not exist (due to, say, filtering)
        Employee firstEmp = empList.stream()
                .min((e1, e2) -> e1.getId() - e2.getId())
                .orElseThrow(NoSuchElementException::new);

        System.out.println("First Emp: " + firstEmp);

        Employee maxSalEmp = empList.stream()
                .max(Comparator.comparing(Employee::getSalary))
                .orElseThrow(NoSuchElementException::new);

        System.out.println("Highest paid employee: " + maxSalEmp);


        System.out.println("\nDistinct operation\n");
        //distinct
        // distinct() does not take any argument and returns the distinct elements in the stream, eliminating duplicates.

        List<Integer> intList = Arrays.asList(2, 5, 3, 2, 4, 3);
        List<Integer> distinctIntList = intList.stream().distinct().collect(Collectors.toList());

        System.out.println("distinct values of intList is: " + distinctIntList);


        System.out.println("\nallMatch, anyMatch, and noneMatch operation\n");
        //allMatch, anyMatch, and noneMatch
        //These operations all take a predicate and return a boolean.
        // Short-circuiting is applied and processing is stopped as soon as the answer is determined:
        List<Integer> intsList = Arrays.asList(2, 4, 5, 6, 8);

        //allMatch() checks if the predicate is true for all the elements in the stream.
        // Here, it returns false as soon as it encounters 5, which is not divisible by 2.
        //
        //anyMatch() checks if the predicate is true for any one element in the stream.
        // Here, again short-circuiting is applied and true is returned immediately after the first element.
        //
        //noneMatch() checks if there are no elements matching the predicate.
        // Here, it simply returns false as soon as it encounters 6, which is divisible by 3.

        boolean allEven = intsList.stream().allMatch(i -> i % 2 == 0);
        boolean oneEven = intsList.stream().anyMatch(i -> i % 2 == 0);
        boolean noneMultipleOfThree = intsList.stream().noneMatch(i -> i % 3 == 0);

        System.out.println("Are all values even: " + allEven);
        System.out.println("Is only one value even: " + oneEven);
        System.out.println("Are all values not a multiple of three: " + noneMultipleOfThree);


        System.out.println("\nmapToInt operation\n");
        //mapToInt
        //geting the greatest employee number

        Integer latestEmpId = empList.stream()
                .mapToInt(Employee::getId)
                .max()
                .orElseThrow(NoSuchElementException::new);



        System.out.println("\naverage operation\n");
        //average()
        Double avgSal = empList.stream()
                .mapToDouble(Employee::getSalary)
                .average()
                .orElseThrow(NoSuchElementException::new);

        System.out.println("Average salary is: " + avgSal);
    }
}
