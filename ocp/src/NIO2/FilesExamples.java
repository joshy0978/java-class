package NIO2;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributeView;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;

public class FilesExamples {

    public static void main(String[] args) throws IOException, InterruptedException {

        Path path1 = Paths.get("src/NIO2/seinfeld.txt");
        Path path2 = Paths.get("src", "NIO2", "seinfeld.txt");

        //isSameFile
        boolean isSameFile = Files.isSameFile(path1, path2);
        System.out.println("isSameFile result: " + isSameFile);


        //Creating a new directory
        Path path = Paths.get("src/NIO2/NewDir");

        try {
            Path newDir = Files.createDirectory(path);
        } catch (FileAlreadyExistsException e) {
            // the directory already exists.
        } catch (IOException e) {
            //something else went wrong
            e.printStackTrace();
        }

        //Copy
        Path sourcePath = Paths.get("src/NIO2/seinfeld.txt");
        Path destinationPath = Paths.get("src/NIO2/seinfeldCopy.txt");

        try {
            Files.copy(sourcePath, destinationPath);
        } catch (FileAlreadyExistsException e) {
            //destination file already exists
        } catch (IOException e) {
            //something else went wrong
            e.printStackTrace();
        }

        try (InputStream is = new FileInputStream("src/NIO2/seinfeld.txt");
             OutputStream out = new FileOutputStream("src/NIO2/seinfeldOutput.txt")) {
            // Copy stream data to file
            Files.copy(is, Paths.get("src/NIO2/seinfeldFromInputStream.txt"));
            // Copy file data to stream
            Files.copy(Paths.get("src/NIO2/seinfeld.txt"), out);
        } catch (IOException e) {
            // Handle file I/O exception... }


        }


        //Move a file
        try {
            Files.move(Paths.get("src/NIO2/seinfeldCopy.txt"), Paths.get("src/NIO2/NewDir/movedFile.txt"));
            Files.move(Paths.get("src/NIO2/seinfeldFromInputStream.txt"), Paths.get("src/NIO2/NewDir/movedOutputFile.txt"));
        } catch (IOException e) {
            // Handle file I/O exception...

        }

        //Delete

        //Creating a new directory
        Path pathToDelete = Paths.get("src/NIO2/DirToDel");

        try {
            Path newDir = Files.createDirectory(pathToDelete);
        } catch (FileAlreadyExistsException e) {
            // the directory already exists.
        } catch (IOException e) {
            //something else went wrong
            e.printStackTrace();
        }


//        System.out.println("Deleting DirToDel");
//        Files.deleteIfExists(Paths.get("src/NIO2/DirToDel"));


        //Reading using files
        Path pathToRead = Paths.get("src/NIO2/seinfeld.txt");
        try (BufferedReader reader = Files.newBufferedReader(pathToRead, Charset.forName("US-ASCII"))) {
            // Read from the stream
            String currentLine = null;
            while ((currentLine = reader.readLine()) != null)
                System.out.println(currentLine);
        } catch (IOException e) {
            // Handle file I/O exception...
        }

        //readAllLines
        try{
            System.out.println("\nReading all Lines \n");
            List<String> lines = Files.readAllLines(pathToRead);
            for (String line : lines) {
                System.out.println(line);
            }
        } catch(IOException e)
        {
            // Handle file I/O exception...
        }

        //Checking metadata of file/dir
        System.out.println("Is pathToRead a Dir: " + Files.isDirectory(pathToRead));
        System.out.println("Is pathToRead a RegularFile: " + Files.isRegularFile(pathToRead));
        System.out.println("Is pathToRead a SymbolicLink: " + Files.isSymbolicLink(pathToRead));

        //Check if file is hidden
        System.out.println("Is file hidden: " + Files.isHidden(Paths.get("src/NIO2/.hiddenfile.txt")));

        //isREadable and isExecutable
        System.out.println("Is pathToRead readable: " + Files.isReadable(pathToRead));
        System.out.println("Is pathToRead a executable: " + Files.isExecutable(pathToRead) );


        //size()
        System.out.println("Size of pathToRead: " + Files.size(pathToRead) + " number of bytes");

        //Last time modified
        System.out.println("pathToRead was last modified: " + Files.getLastModifiedTime(pathToRead));

        //Set last time modified
        long time = System.currentTimeMillis();
        FileTime fileTime = FileTime.fromMillis(time);
        Files.setLastModifiedTime(pathToRead, fileTime);
        //Last time modified
        System.out.println("pathToRead was last modified: " + Files.getLastModifiedTime(pathToRead));

        //owner
        System.out.println("Who is the owner of pathToRead: " + Files.getOwner(pathToRead));



        //Views

        //readonly
        BasicFileAttributes attr = Files.readAttributes(Paths.get("src/NIO2/.hiddenfile.txt"), BasicFileAttributes.class);

        System.out.println("creationTime: " + attr.creationTime());
        System.out.println("lastAccessTime: " + attr.lastAccessTime());
        System.out.println("lastModifiedTime: " + attr.lastModifiedTime());

        //read and edit
        BasicFileAttributeView fileAttributeView =
                Files.getFileAttributeView(Paths.get("src/NIO2/.hiddenfile.txt"), BasicFileAttributeView.class);
        //get basic attributes
        BasicFileAttributes basicFileAttributes = fileAttributeView.readAttributes();
        System.out.println(basicFileAttributes.creationTime());
        //modify times
        FileTime from = FileTime.from(400, TimeUnit.HOURS);
        fileAttributeView.setTimes(from, from, from);
        //read last modify
        FileTime lastModifiedTime = Files.getLastModifiedTime(Paths.get("src/NIO2/.hiddenfile.txt"));
        System.out.println(lastModifiedTime);





        //Walking and traversing
        Path startingPath = Paths.get("src/NIO2/");
        try {
            Files.walk(startingPath)
                    .filter(p -> p.toString().endsWith(".java")) .forEach(System.out::println);
        } catch (IOException e) {
        // Handle file I/O exception...
        }


        long dateFilter = 1420070400000l;

        try {
            //p = Path     a = BasicFileAttributes
            Stream<Path> stream = Files.find(startingPath, 10, (p, a) -> p.toString().endsWith(".java")
                    && a.lastModifiedTime().toMillis()>dateFilter);
            stream.forEach(System.out::println);
        } catch (Exception e) {
// Handle file I/O exception...
        }


        //list dir
        try {
            Files.list(startingPath).filter(p -> !Files.isDirectory(p)) .map(p -> p.toAbsolutePath()).forEach(System.out::println);
        } catch (IOException e) {
// Handle file I/O exception...
        }




    }


}

