package NIO2;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.*;
import java.util.Arrays;

public class PathExample {

    public static void main(String[] args) throws URISyntaxException, IOException {

        //Create a Path Object
        Path path = Paths.get("/src/NIO2/seinfeld.txt");
        System.out.println(path.getFileName());

        System.out.println(path.getParent());

        //Creating a Path using Varargs of Strings
        Path pathUsingVargs = Paths.get("src", "NIO2", "seinfeld.txt");
        File file = pathUsingVargs.toFile();
        System.out.println(file.exists());

        Path currentDirectory = Paths.get(".");
        System.out.println(currentDirectory.toAbsolutePath());

        Path parentDirectory = Paths.get("..");
        System.out.println(parentDirectory.toAbsolutePath());

        String stringOfPath = currentDirectory.toString();
        Path pathFromString = Paths.get(stringOfPath);
        System.out.println(pathFromString.toAbsolutePath());

        //Normalize a path
        //Normalizing a path means to remove all the .. and .
        Path normalizedPath = pathFromString.normalize();
        System.out.println("Normalized Path" +normalizedPath.toAbsolutePath());

        //Creating a Path from a URI (universal resource identifier)
        Path pathFromURI = Paths.get(new URI("file:////Users/joshuataylor/git/java-class/ocp/src/NIO2"));
        File isValidPath = pathFromURI.toFile();
        System.out.println("isValidPath: " + isValidPath.isDirectory());

        //Connecting to an FTP server
//        Path path5 = Paths.get(new URI("ftp://demo:password@ftp.test.rebex.net"));
////        File ftpFiles = path5.toFile();
////        System.out.println(Arrays.toString(ftpFiles.listFiles()));



        Path path1 = FileSystems.getDefault().getPath("/src/NIO2/seinfeld.txt");
        System.out.println(path1.getFileName());
        System.out.println(path1.toAbsolutePath());
        System.out.println("Path1 parent: " + path1.getParent());
        System.out.println("Root: " + path1.getRoot());
        System.out.println("The Path Name is: " + path1);
        for(int i =0;   i<path1.getNameCount(); i++) {
            System.out.println("Element " + i + " is: " + path1.getName(i));
        }


        Path path2 = FileSystems.getDefault().getPath("src","NIO2","seinfeld.txt");
        System.out.println(path2.getFileName());
        System.out.println("Convert relative path to absolute path: " + path2.toAbsolutePath());

        //subpath
        System.out.println("Subpath from 0 to 3 is " + path2.subpath(0,3));
        System.out.println("Subpath from 1 to 3 is " + path2.subpath(1,3));
        System.out.println("Subpath from 2 to 3 is " + path2.subpath(2,3));



        //relativize
        Path rel1 = Paths.get("/src/NIO2/seinfeld.txt");
        Path rel2 = Paths.get("/Users/joshuataylor/git/java-class/ocp/src/NIO2/jrrrtolkien.txt");
        System.out.println(rel1.relativize(rel2) + " " + rel2.relativize(rel1));


        //resolve
        Path currentDirectory1 = Paths.get(".");
        Path jrrrPath = Paths.get("jrrrtolkien.txt");
        System.out.println(currentDirectory1.resolve(jrrrPath));
        System.out.println(jrrrPath.resolve(currentDirectory1));


        //To Real Path
        Path real_path = rel2.toRealPath(LinkOption.NOFOLLOW_LINKS);
        System.out.println("real_path: " + real_path);

        //isSameFile
        boolean isSameFile = Files.isSameFile(pathUsingVargs, path);
        System.out.println("isSameFile result: " + isSameFile);





    }
}
