package Chapter3.ComparatorvsComparable;

//Java Program to demonstrate the use of Java Comparable.
//Creating a class which implements Comparable Interface
import java.util.*;


class Student implements Comparable<Student>{
    int rollNumber;
    String name;
    int age;
    Student(int rollNumber, String name, int age){
        this.rollNumber = rollNumber;
        this.name=name;
        this.age=age;
    }
    public int compareTo(Student st){
        if(age==st.age)
            return 0;
        else if(age>st.age)
            return 1;
        else
            return -1;
    }
}
//Creating a test class to sort the elements
class TestComparableSort{
    public static void main(String args[]){
        ArrayList<Student> al=new ArrayList<Student>();
        al.add(new Student(101,"Vijay",23));
        al.add(new Student(106,"Ajay",27));
        al.add(new Student(105,"Jai",21));

        Collections.sort(al);
        for(Student st:al){
            System.out.println(st.rollNumber +" "+st.name+" "+st.age);
        }
    }
}
