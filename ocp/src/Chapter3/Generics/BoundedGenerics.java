package Chapter3.Generics;

import java.util.ArrayList;
import java.util.List;

class GenericBounded {
    public static void main(String args[]) {
        BoxNumber<Integer> BoxNumber1 = new BoxNumber<Integer>(new Integer(6), new Integer(5), new Integer(3));
        BoxNumber<Integer> BoxNumber2 = new BoxNumber<Integer>(7, 2, 10); // autoBoxNumbering parameters
        BoxNumber<Float> BoxNumber3 = new BoxNumber<Float>(4.25f, 3.0f, 10.0f);
        BoxNumber<Double> BoxNumber4 = new BoxNumber<Double>(11.5, 5.0, 6.5);
        BoxNumber<Byte> BoxNumber5 = new BoxNumber<Byte>((byte)0xA, (byte)0x13, (byte)0xB);

        System.out.println("The volume of BoxNumber1 is: " + BoxNumber1.getVolume());
        System.out.println("The volume of BoxNumber2 is: " + BoxNumber2.getVolume());
        System.out.println("The volume of BoxNumber3 is: " + BoxNumber3.getVolume());
        System.out.println("The volume of BoxNumber4 is: " + BoxNumber4.getVolume());
        System.out.println("The volume of BoxNumber5 is: " + BoxNumber5.getVolume());
    }
}

class BoxNumber<T extends Number> {
    private T length;
    private T height;
    private T width;

    BoxNumber(T length, T height, T width) {
        this.length = length;
        this.height = height;
        this.width = width;
    }

    double getVolume () {
        return (length.doubleValue() * height.doubleValue() * width.doubleValue());
    }


    public static <T extends Comparable<T>> int compare(T t1, T t2){
        return t1.compareTo(t2);
    }

}



class BoundedExamples {

    /******
     * Upper bounded wildcards are used to relax the restriction on the type of variable in a method.
     *
     * We use generics wildcard with extends keyword and the upper bound class or interface
     * that will allow us to pass argument of upper bound or it’s subclasses types.
     * <? extends Type>
     */
    public static double sum(List<? extends Number> list){
        double sum = 0;
        for(Number n : list){
            sum += n.doubleValue();
        }
        return sum;
    }


    /************************************
     * Unbounded Wildcard
     * Sometimes we have a situation where we want our generic method to be working with all types,
     * in this case unbounded wildcard can be used. Its same as using <? extends Object>.
     */
    public static void printData(List<?> list){
        for(Object obj : list){
            System.out.print(obj + "::");
        }
        System.out.println();
    }


    /*******************************
     *Lower bounded Wildcard
     *Suppose we want to add Integers to a list of integers in a method, we can keep the argument
     * type as List<Integer> but it will be tied up with Integers whereas List<Number> and List<Object>
     * can also hold integers, so we can use lower bound wildcard to achieve this.
     * We use generics wildcard (?) with super keyword and lower bound class to achieve this.
     */
    public static void addIntegers(List<? super Integer> list){
        list.add(new Integer(50));
    }

    public static void main(String[] args) {

        List<Integer> integerList = new ArrayList<>();
        integerList.add(10);
        integerList.add(4);
        integerList.add(1);
        integerList.add(9);

        List<Double> doubleList = new ArrayList<>();
        doubleList.add(10.0);
        doubleList.add(4.0);
        doubleList.add(1.0);
        doubleList.add(9.0);


        System.out.println(BoundedExamples.sum(integerList));
        System.out.println(BoundedExamples.sum(doubleList));

        BoundedExamples.printData(integerList);
        BoundedExamples.printData(doubleList);

        BoundedExamples.addIntegers(integerList);

        //Does not compile because Double does not
        //inherit from Integer
        //BoundedExamples.addIntegers(doubleList);

    }



}
