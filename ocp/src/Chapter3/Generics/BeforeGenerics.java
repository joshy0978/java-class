package Chapter3.Generics;

class CrateString {
    private String contents;
    public String emptyCrate() {
        return contents;
    }
    public void packCrate(String contents) {
        this.contents = contents;
    }
}

class CrateInteger {
    private Integer contents;
    public Integer emptyCrate() {
        return contents;
    }
    public void packCrate(Integer contents) {
        this.contents = contents;
    }
}

class CrateDouble {
    private Double contents;
    public Double emptyCrate() {
        return contents;
    }
    public void packCrate(Double contents) {
        this.contents = contents;
    }
}

class CustomObject {
}

class CrateObjectClass {
    private CustomObject contents;
    public CustomObject emptyCrate() {
        return contents;
    }
    public void packCrate(CustomObject contents) {
        this.contents = contents;
    }
}

interface CompareString{

    boolean compareString(String a, String b);

}

interface CompareIntegers{

    boolean compareIntegers(Integer a, Integer b);

}

class BeforeGenericsMain {


    public static void main(String[] args) {

        CrateDouble crateDouble = new CrateDouble();

        CrateInteger crateInteger = new CrateInteger();

        CrateString crateString = new CrateString();

        CrateObjectClass crateObjectClass = new CrateObjectClass();




    }



}


