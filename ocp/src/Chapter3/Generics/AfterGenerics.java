package Chapter3.Generics;

//T = Type

class Crate<T> {
    private T contents;
    public T emptyCrate() {
        return contents;
    }
    public void packCrate( T contents) {
        this.contents = contents;
    }

}

class Box<T, S> {
    private T first_item;
    private S second_item;

    public void add(T t, S s) {
        this.first_item = t;
        this.second_item = s;
    }

    public T getFirst() {
        return first_item;
    }

    public S getSecond() {
        return second_item;
    }
}

interface Compare<T> {

    boolean compare(T a, T b);

}


class AfterGenericsMain {


    public static void main(String[] args) {

        Crate<Double> crateDouble = new Crate<>();

        Crate<Integer>  crateInteger = new Crate<>();

        Crate<String>  crateString = new Crate<>();

        Crate<CustomObject>  crateObjectClass = new Crate<>();

        Box<String, CustomObject> box = new Box<>();

    }



}

