package Chapter3.Arrays;

import java.util.Arrays;

public class Array {

    public static void main(String[] args) {

        int[] primes = new int[10]; // elements will be initialize with default value
        int[] evens = new int[]{2, 4, 6, 8}; // inline initialization
        int[] odds = {1, 3, 5};
        int[][] cubes = {
                {1, 1},
                {2, 4},
                {3, 9},
                {4, 16}
        };

        // looping over array using for loop
        // access element of array using []
        for(int i =0; i< odds.length; i++){
            System.out.printf("element at index %d is %d %n", i, odds[i]);
        }

// iterating over array using enhanced for loop
        for(int even : evens){
            System.out.println("current number is " + even);
        }

        boolean isEqual = Arrays.equals(primes, odds);

        if (isEqual) {
            System.out.printf("array %s and %s are equal %n",
                    Arrays.toString(primes),
                    Arrays.toString(odds));
        } else {
            System.out.printf("array %s and %s are not equal %n",
                    Arrays.toString(primes),
                    Arrays.toString(odds));
        }

    }
}
