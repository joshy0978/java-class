package Chapter3.GenericsContainers;

import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Queue;

public class Queues {

    public static void main(String[] args) {

        /*********
         * Queue is an interface
         * LinkedList and PriorityQueue all implement List
         * Therefore, a List reference can point to any of these container classes
         *
         * Try it for yourself. Try setting list to different collections. Only one reference
         * can be uncommented at a time.
         *
         * Remember a queue is a first in, first out data structure
         *
         *  C -> B -> A -> {Queue} -> C -> B -> A
         */

        Queue<String> queue = new LinkedList<String>();
        //Queue<String> queue = new PriorityQueue<String>();

        //Add element to queue
        queue.add("element 1");
        queue.add("element 2");
        queue.add("element 3");

        //Get but done remeber element at top of queue
        String peekedElement = queue.element(); // will throw exception if queue is empty

        peekedElement = queue.peek(); // will return null if queue is empty

        //Remove element at top of queue
        String removedElement = queue.remove();







    }
}
