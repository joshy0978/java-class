package Chapter3.GenericsContainers;

import java.util.*;

public class Sets {

    public static void main(String[] args) {

        /*********
         * Set is an interface
         * Hastset, LinkedHastSet, and Treeset all implement List
         * Therefore, a Set reference can point to any of these container classes
         *
         * Try it for yourself. Try setting list to different collections. Only one reference
         * can be uncommented at a time.
         *
         * Remember that a set doesn't contain duplicates
         */

        Set<String> set = new HashSet<>();
        //Set<String> set = new LinkedHashSet<>();
        //Set<String> set = new TreeSet<>();


        //Add Element To Set
        set.add("element 1");
        set.add("element 1"); //value is not added to set because sets do not allow duplicates
        set.add("element 2");
        set.add("element 3");
        System.out.println( set.contains("element 1") );

        //Remove Element from set
        set.remove("element 1");

        //Remove all elements from set
        set.clear();

        //Add elements from other set
        Set newSet = new HashSet();
        newSet.add("one");
        newSet.add("two");
        newSet.add("three");

        set.addAll(set);

        //Remove all items from other set
        set.removeAll(newSet);

        //Get number of elements in set aka size
        int size = set.size();
        System.out.println("Size of set: " + size);

        //Check if set is empty
        boolean isEmpty = set.isEmpty();
        System.out.println("Is set empty: " + isEmpty);


        //Check if set contains element
        boolean contains123 = set.contains("123");
        System.out.println("Does set contain '123'" + contains123);


    }
}
