package Chapter3.GenericsContainers;

import Chapter3.Arrays.Array;

import java.util.*;

public class Lists {

    public static void main(String[] args) {

        /*********
         * List is an interface
         * ArrayList, LinkedList, Vector, and Stack all implement List
         * Therefore, a List reference can point to any of these container classes
         *
         * Try it for yourself. Try setting list to different collections. Only one reference
         * can be uncommented at a time.
         */

        List<String> list = new ArrayList<>();
        //List<String> list= new LinkedList<>();
        //List<String> list = new Vector<>();
        //List<String> list = new Stack<>();


        //Insert Elements in a Java List
        list.add("element 1");
        list.add("element 2");
        list.add("element 3");

        //Insert Elements at Specific Index
        list.add(0, "element 4");

        //Insert All Elements From One List Into Another

        List<String> listSource = new ArrayList<>();

        listSource.add("other list element 1");
        listSource.add("other list element 2");

        list.addAll(listSource);


        //Get Elements From a Java List
        //access via index
        String element0 = list.get(0);
        String element1 = list.get(1);
        String element3 = list.get(2);
        System.out.println("element0: " + element0);
        System.out.println("element1: " + element1);
        System.out.println("element3: " + element3);

        //Find Elements in a List

        //You can find elements in a Java List using one of these two methods:
        //indexOf()
        //lastIndexOf()
        int index1 = list.indexOf(element1);
        int index2 = list.indexOf(element3);

        System.out.println("index1 = " + index1);
        System.out.println("index2 = " + index2);

        //Find Last Occurrence of Element in a List
        int lastIndex = list.lastIndexOf(element1);
        System.out.println("lastIndex = " + lastIndex);

        //Checking if List Contains Element
        boolean containsElement = list.contains("element 1");
        System.out.println(containsElement);

        //Removing element from list
        list.remove(element0);  //using the element removes the first occurrence
        list.remove(0);   //using the element removes the element at that index..index are zero based

        //Get size of the list
        //Gives you the number of elements
        int size = list.size();
        System.out.println("Size of list is: " + size);

        //Get a sublist of a list
        List sublist = list.subList(1, 3);
        System.out.println("sublist: " + sublist);

        //Remove All Elements From a Java List









    }
}
