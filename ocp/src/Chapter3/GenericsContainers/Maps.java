package Chapter3.GenericsContainers;

import java.util.HashMap;

/**********************
 * The HashMap is a data structure, based on hashing,
 * which allows you to store an object as a key-value pair,
 * an advantage of using HashMap is that you can retrieve
 * object on constant time i.e. O(1) if you know the key.
 */

public class Maps {

    public static void main(String[] args) {

        HashMap<Integer, String> cache = new HashMap<Integer, String>();

        //Copy map to another map
        HashMap<Integer, String> map = new HashMap(cache);

        //Add to map
        map.put(21, "Twenty One");

        //Get Value from key
        Integer key = 21;
        String value = map.get(key);
        System.out.println("Value from map: " + value);

        //Check if map contains either key or value
        System.out.println("Does HashMap contains 21 as key: " + map.containsKey(21));
        System.out.println("Does HashMap contains 21 as value: " + map.containsValue(21));
        System.out.println("Does HashMap contains Twenty One as value: " + map.containsValue("Twenty One"));

        //Check if map is empty
        boolean isEmpty = map.isEmpty();
        System.out.println("Is HashMap is empty: " + isEmpty);

        //Remove Value based on key
        key = 21;
        value = map.remove(key);
        System.out.println("Following value is removed from Map: " + value);
    }
}
