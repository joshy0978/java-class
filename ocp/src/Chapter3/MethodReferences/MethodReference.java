package Chapter3.MethodReferences;

import com.sun.xml.internal.ws.util.StringUtils;

import java.util.*;
import java.util.function.Supplier;

/************************
 * There are four kinds of method references:
 *
 * Static methods
 * Instance methods of particular objects
 * Instance methods of an arbitrary object of a particular type
 * Constructor
 */

public class MethodReference {

    public static void print(final int number) {
        System.out.println("I am printing: " + number);
    }

    private static class MyComparator {
        public int compare(final Integer a, final Integer b) {
            return a.compareTo(b);
        }
    }

    private static class Person {
        private String name;
        public Person(final String name) {
            this.name = name;
        }
        public void printName() {
            System.out.println(name);
        }
    }


    public static void main(String args[]) {
        List<Integer> list = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);


        /*****
         * Rference to a Static Method
         */
        // Method reference
        list.forEach(MethodReference::print);
        // Lambda expression
        list.forEach(number -> MethodReference.print(number));
        // normal
        for(int number : list) {
            MethodReference.print(number);
        }


        /********
         * Reference to an Instance Method of a Particular Object
         */
        MethodReference.MyComparator myComparator = new MyComparator();
        // Method reference
        Collections.sort(list, myComparator::compare);

        // Lambda expression
        Collections.sort(list, (a, b) -> myComparator.compare(a,b));


        /********
         * Reference to an Instance Method of an
         * Arbitrary Object of a Particular Type
         */
        final List<Person> people = Arrays.asList(new Person("dan"), new Person("laura"));
        // Method reference
        people.forEach(Person::printName);
        // Lambda expression
        people.forEach(person -> person.printName());
        // normal
        for (final Person person : people) {
            person.printName();
        }

        /*******
         * Reference to a Constructor
         */
        // Method Reference
        copyElements(list, ArrayList::new);
        // Lambda expression
        copyElements(list, () -> new ArrayList());

    }

    private static void copyElements(final List<Integer> list, final Supplier<Collection<Integer>> targetCollection) {
        // Method reference to a particular instance
        list.forEach(targetCollection.get()::add);

        // Lambda example
        //list.forEach((e) -> targetCollection.get().add((e)));
    }


}
