package Chapter2.Interface;


import java.nio.file.Path;

interface Vehicle {

    static final int default_speed = 20;

    // all are the abstract methods.
    void changeGear(int a);
    void speedUp(int a);
    void applyBrakes(int a);

    //default Method
    default void display()
    {
        System.out.println("display from Vehicle interface");
    }

    //static method
    static void speed(){
        System.out.println("The default speed for a vehicle is: " + Vehicle.default_speed);
    }

}


//Bicycle Class
class Bicycle implements Vehicle{

    int speed;
    int gear;

    // to change gear
    @Override
    public void changeGear(int newGear){

        //delay 500ms before changing
        gear = newGear;
    }

    // to increase speed
    @Override
    public void speedUp(int increment){

        speed = speed + increment;
    }

    // to decrease speed
    @Override
    public void applyBrakes(int decrement){

        speed = speed - decrement;
    }

    public void printStates() {
        System.out.println("speed: " + speed
                + " gear: " + gear);
    }
}

class Bike implements Vehicle {

    int speed;
    int gear;

    // to change gear
    @Override
    public void changeGear(int newGear){

        //zero delay
        gear = newGear;
    }

    // to increase speed
    @Override
    public void speedUp(int increment){

        speed = speed + increment;
    }

    // to decrease speed
    @Override
    public void applyBrakes(int decrement){

        speed = speed - decrement;
    }

    public void printStates() {
        System.out.println("speed: " + speed
                + " gear: " + gear);
    }

    @Override
    public void display() {
        if(false){
        Vehicle.super.display();

        } else {

            System.out.println("Displaying from Bike Class");
        }
    }
}
class GFG {

    public static void main (String[] args) {

        Vehicle.speed();

        // creating an inatance of Bicycle
        // doing some operations
        Bicycle bicycle = new Bicycle();
        bicycle.changeGear(2);
        bicycle.speedUp(3);
        bicycle.applyBrakes(1);
        bicycle.display();

        System.out.println("Bicycle present state :");
        bicycle.printStates();

        // creating instance of bike.
        Bike bike = new Bike();
        bike.changeGear(1);
        bike.speedUp(4);
        bike.applyBrakes(3);
        bike.display();

        System.out.println("Bike present state :");
        bike.printStates();



        Vehicle v = bicycle;

        v.changeGear(4);
        v.applyBrakes(1);
        v.speedUp(10);
        v.display();


        v = bike;
        v.changeGear(4);
        v.applyBrakes(1);
        v.speedUp(10);
        v.display();




    }
}
