package Chapter2.Interface;

import java.util.function.Predicate;

/********************
 * interface <interface_name> {
 *
 *     // declare constant fields
 *     // declare methods that abstract
 *     // by default.
 * }
 */

// Java program to demonstrate working of
// interface.

// A simple interface
@FunctionalInterface
interface Player
{
    // public, static and final
    static final int a = 10;

    static void staticPlayerMethod(){
        System.out.println("staticPlayerMethod");
    }

    // public and abstract
    void display();

    default void sayHello() {
        System.out.println("Say Hello");
    }
}

// A class that implements interface.
class testClass implements Player
{
    // Implementing the capabilities of
    // interface.
    public void display()
    {
        System.out.println("Player Display");
    }

    @Override
    public void sayHello() {
        Player.super.sayHello();
        System.out.println("Custom Say Hello in testClass");
        System.out.println(Player.a);
    }

    // Driver Code
    public static void main (String[] args)
    {
        testClass t = new testClass();
        t.display();
        System.out.println(Player.a);
        Player.staticPlayerMethod();
        t.sayHello();
    }
}
