package Chapter2.DesignPatterns;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

final class Student {
    private final String name;
    private final int age;
    private final LocalDate dateJoined;
    private final List<Student> friends;

    public Student(String name, int age, LocalDate dateJoined, List<Student> friends) {
        this.name = name;
        this.age = age;
        this.dateJoined = dateJoined;
        this.friends = new ArrayList<>(friends);
    }

    public Student(String name, int age, LocalDate dateJoined) {
        this.name = name;
        this.age = age;
        this.dateJoined = dateJoined;
        this.friends = new ArrayList<>();
    }

    public String getName() {
        return name;
    }
    public int getAge() {
        return age;
    }
    public LocalDate getDateJoined() {
        return dateJoined;
    }
    public List<Student> getFriends() {return this.friends;};

    @Override
    public String toString() {
        return "Student name: " + this.name + " and age is: " + this.age;
    }

    @Override
    public boolean equals(Object obj) {

        if(!(obj instanceof Student))
            return false;

        Student otherStudent = (Student)obj;

        return name.equals(otherStudent.name) &&
                age == otherStudent.age &&
                dateJoined.equals(otherStudent.dateJoined);
    }
}
class TestImmutable {
    public static void main(String[] args) {

        List<Student> friends = new ArrayList<>();
        friends.add(new Student("Tom", 19, LocalDate.of(2014, 4, 1)));
        friends.add(new Student("John", 10, LocalDate.of(2018, 3, 6)));
        friends.add(new Student("Peter", 114, LocalDate.of(2015, 6, 5)));

        Student original = new Student("Yogen", 23, LocalDate.of(2016, 5, 1), friends);
        original.getDateJoined().plusYears(2);
        Student expected = new Student("Yogen", 23, LocalDate.of(2016, 5, 1));
        System.out.println("Student are equal: " + original.equals(expected));

        List<Student> friendsOfYogen = original.getFriends();
        friendsOfYogen.set(2, new Student("Ops", 21, LocalDate.of(2123, 2, 12)));

        System.out.println(original.getFriends().get(2));
    }
}
