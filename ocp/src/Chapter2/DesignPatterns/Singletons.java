package Chapter2.DesignPatterns;

/********************
 * Eager Initialization
 *
 * In eager initialization, the instance of Singleton Class is
 * created at the time of class loading, this is the easiest method
 * to create a singleton class but it has a drawback that instance
 * is created even though client application might not be using it.
 */


class EagerInitializedSingleton {

    String userName = "Static Block Name";

    private static final EagerInitializedSingleton instance = new EagerInitializedSingleton();

    //private constructor to avoid client applications to use constructor
    private EagerInitializedSingleton(){}

    public static EagerInitializedSingleton getInstance(){
        return instance;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}

/*****************
 * Static block initialization
 *
 * Static block initialization implementation is similar to eager initialization,
 * except that instance of class is created in the static block that provides option for exception handling.
 */

class StaticBlockSingleton {

    String userName = "Static Block Name";

    private static StaticBlockSingleton instance;

    private StaticBlockSingleton(){}

    //static block initialization for exception handling
    static{
        try{
            instance = new StaticBlockSingleton();
        }catch(Exception e){
            throw new RuntimeException("Exception occured in creating singleton instance");
        }
    }

    public static StaticBlockSingleton getInstance(){
        return instance;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}

/*******************
 * Lazy Initialization
 *
 * Lazy initialization method to implement Singleton pattern creates the instance in the global access method.
 * Here is the sample code for creating Singleton class with this approach.
 */

class LazyInitializedSingleton {

    String userName;

    private static LazyInitializedSingleton instance;

    private LazyInitializedSingleton(){
        userName = "Default";
    }

    public static LazyInitializedSingleton getInstance(){
        if(instance == null){
            instance = new LazyInitializedSingleton();
        }
        return instance;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}

class A {

    void changeUserName(){
        LazyInitializedSingleton lazyInitializedSingleton = LazyInitializedSingleton.getInstance();
        System.out.println(lazyInitializedSingleton.getUserName());
        lazyInitializedSingleton.setUserName("Set From Class A");
    }

}

class SingletonMain {

    public static void main(String[] args) {

        LazyInitializedSingleton lazyInitializedSingleton = LazyInitializedSingleton.getInstance();
        System.out.println("UserName: " + lazyInitializedSingleton.getUserName());

        System.out.println("-----------------------------------------------");
        System.out.println("Changing Username from class A");
        A classA = new A();
        classA.changeUserName();

        System.out.println("Leaving Class A");
        System.out.println("-----------------------------------------------");


        System.out.println("UserName: " + LazyInitializedSingleton.getInstance().getUserName());


    }
}
