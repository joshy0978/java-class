package Chapter2;

class Car {
    // Methods implementation and class/Instance members
    private String color;
    private int maxSpeed;
    public void carInfo(){
        System.out.println("Car Color= "+color + " Max Speed= " + maxSpeed);
    }
    public void setColor(String color) {
        this.color = color;
    }
    public void setMaxSpeed(int maxSpeed) {
        this.maxSpeed = maxSpeed;
    }
}


//Maruti IS-A Car
class Maruti extends Car{
    //Maruti extends Car and thus inherits all methods from Car (except final and static)
    //Maruti can also define all its specific functionality
    Engine marutiEngine;

    public Maruti(Engine marutiEngine) {
        this.marutiEngine = marutiEngine;
    }

    public Engine getMarutiEngine() {
        return marutiEngine;
    }

    public void marutiStartDemo(){
        marutiEngine.start();
    }

    public void marutiStopDemo(){
        marutiEngine.stop();
    }
}


class Engine {
    public void start(){
        System.out.println("Engine Started:");
    }
    public void stop(){
        System.out.println("Engine Stopped:");
    }
}

class RelationsDemo {
    public static void main(String[] args) {
        Engine engine = new Engine();
        Maruti myMaruti = new Maruti(engine);
        myMaruti.setColor("RED");
        myMaruti.setMaxSpeed(180);
        myMaruti.carInfo();
        myMaruti.marutiStartDemo();
    }

}
