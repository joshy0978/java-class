package Chapter2.FunctionalInterface;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

interface func{

    int function(int a, int b);

}



class Addfunction implements func {

    @Override
    public int function(int a, int b) {
        return a + b;
    }
}

class Multfunction implements func {

    @Override
    public int function(int a, int b) {
        return a * b;
    }
}

class Subfunction implements func {

    @Override
    public int function(int a, int b) {
        return a - b;
    }
}

class AbstractfuncMain {

    public static void main(String[] args) {


        int a = 45;
        int b = 10;

        // add
        func addfunction = new Addfunction();
        int addResult = addfunction.function(a, b);

        // mult
        func multFunction = new Multfunction();
        int multResult = multFunction.function(a, b);

        //div
        func divfunction = new func() {
            @Override
            public int function(int a, int b) {
                return a/b;
            }
        };

        int divRsult = divfunction.function(a,b);

        func modFunction = (i,j) -> (i % j);
        int modResult = modFunction.function(a, b);

        List<String> list = new ArrayList<>();
        list.add("Josh");
        list.add("Kevin");

        System.out.println(list.stream()
                .anyMatch(s -> s.contains("K")));

        Consumer<String> printConsumer = new Consumer<String>() {
            @Override
            public void accept(String s) {
                System.out.println(s);
            }
        };

        Printconsumer p = new Printconsumer();

        list.forEach(s-> System.out.println(s));

        for (String s: list){
            System.out.println(s);
        }


    }



}

class Printconsumer implements Consumer<String> {

    @Override
    public void accept(String s) {
        System.out.println(s);
    }
}
