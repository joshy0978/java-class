package Chapter6.TryWith;

public class PrePostConditions {


    public static void main(String[] args) {

        BankAccount bankAccount = new BankAccount("Josh", 65432456);

        //Preconditon
        assert(bankAccount.name.equals("Josh"));

        bankAccount.setName("Taylor");

        //Post Condition
        assert("Taylor".equals(bankAccount.getName()));





    }
}
