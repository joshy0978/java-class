package Chapter6.TryWith;

import java.io.*;
import java.nio.file.NoSuchFileException;

public class CustomReader implements AutoCloseable {

    private String path;
    private BufferedReader reader;

    CustomReader(String path) {

        assert(!path.isEmpty());
        this.path = path;
    }

    public void readFile() throws IOException {

        this.reader = new BufferedReader(new FileReader(path));
        StringBuilder builder = new StringBuilder();
        String currentLine = reader.readLine();
        while (currentLine != null) {
            builder.append(currentLine);
            builder.append("\n");
            currentLine = reader.readLine();
        }

        System.out.println(builder.toString());

    }

    @Override
    public void close() throws IOException {
        reader.close();
    }
}
