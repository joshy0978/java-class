package Chapter6.TryWith;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class TryWithMain {

    public static void main(String[] args) throws IOException {

        Path inputPath = Paths.get("src/Chapter6/TryWith/senfieldipsum.txt");

        Path output1 = Paths.get("src/Chapter6/TryWith/output1.txt");

        Path output2 = Paths.get("src/Chapter6/TryWith/output2.txt");

        CopyFile.beforeTryWith(inputPath, output1);
        CopyFile.afterTryWith(inputPath, output2);


        try(CustomReader customReader = new CustomReader("src/Chapter6/TryWith/senfieldipsum.txt")){
            customReader.readFile();
        }


        assert(false);



    }
}
