package Chapter6.TryWith;

class InsufficientFunds extends RuntimeException {

    public InsufficientFunds() {
        super("Insufficient Funds");
    }

    public InsufficientFunds(String message) {
        super(message);
    }
}

class InsufficientFundsChecked extends Exception {

    public InsufficientFundsChecked() {
        super("Insufficient Funds");
    }

    public InsufficientFundsChecked(String message) {
        super(message);
    }
}

