package Chapter6.TryWith;

class BankAccount {


    String name;
    int accountNumber;
    int balance = 0;

    public BankAccount(String name, int accountNumber) {
        assert(validateName(name));
        assert(validateAccountNumber(accountNumber));
        this.name = name;
        this.accountNumber = accountNumber;
    }

    public int getAccountNumber() {
        if (balance == 0) {
            throw new InsufficientFunds();
        }
        return accountNumber;
    }

    private boolean validateName(String name){
        if(name.equals("")){
            return false;
        }
        return true;

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private boolean validateAccountNumber(int accNum){
        if(accNum > 0 && accNum < 999999999){
            return true;
        }
        return false;
    }




}
