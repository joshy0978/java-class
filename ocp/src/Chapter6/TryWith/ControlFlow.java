package Chapter6.TryWith;

import static Chapter6.TryWith.Seasons.FALL;
import static Chapter6.TryWith.Seasons.SPRING;
import static Chapter6.TryWith.Seasons.SUMMER;

public class ControlFlow {

    public static void main(String[] args) {

        Seasons season = Seasons.SPRING;

        switch (season) {
            case SPRING:
            case FALL:
                System.out.println("Shorter hours");
                break;
            case SUMMER:
            case WINTER:
                System.out.println("Longer hours");
                break;
            default:
            assert false: "Invalid season";
        };

    }
}
